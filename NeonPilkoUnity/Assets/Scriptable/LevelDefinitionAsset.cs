﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelDefinitionAsset", menuName = "Novulo/LevelDefinition", order = 1)]
public class LevelDefinitionAsset : ScriptableObject
{
	#region Members

	public float FrequencyMin;
	public float FrequencyMax;
	public float FrequencyIncrement;

	public float SpeedMin;
	public float SpeedMax;
	public float SpeedIncrement;

	#endregion
}

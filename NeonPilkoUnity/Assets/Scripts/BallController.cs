﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class BallController : MonoBehaviour
{
    #region Events

    public delegate void ScoreUpdateCallback(int score);
    public static ScoreUpdateCallback OnScoreUpdate;

    public delegate void LifeUpdateCallback(int remaining);
    public static LifeUpdateCallback OnLifeUpdate;

    #endregion


    #region Members

    public float AnimationTime;
    public float RotationSpeed;
    public float BlockSize;
   
    public Transform BallPosition;

    private int m_sessionScore = 0;
    private int m_sessionLife = 1;
    private float m_timer = 0f;
    private bool m_ignoreHit = false;
    private Types.GridPosition m_currentPos = Types.GridPosition.Noof;
    private Types.GridPosition m_targetPos = Types.GridPosition.Noof;

    private Dictionary<Types.GridPosition, Vector3> GridPositions;
    private Dictionary<Types.GridPosition, List<Types.ESwipeDirection>> m_gridAllowedConnections;
    private Dictionary<Types.GridPosition, Types.GridPosition[]> m_gridLinkConnections;

    private static BallController ms_instance = null;

    private AudioSource m_source = null;

	#endregion


	#region Properties

	public static BallController Instance { get { return ms_instance; } }

	public int SessionScore { get { return m_sessionScore; } }
	public Types.GridPosition CurrentPosition { get { return m_currentPos; } }
	public bool BlockSwipe { get; set; } = false;
	public bool IsNewBestScore { get; private set; } = false;

	private float GlobalTime { get; set; } = 1f;

	#endregion


	#region MonoBehaviour

	void Awake ()
    {
        GameManager.OnGameStarted += GameStarted;
        GameManager.OnGameEnd += GameEnded;
        GameManager.OnGameReset += GameReset;
        PauseMenuController.OnContinuePressed += ContinuePressed;
        SettingsController.OnSFXSetting += SFXSetting;
        //
        ms_instance = this;
        m_source = GetComponent<AudioSource>();
	}


    private void OnDestroy()
    {
        GameManager.OnGameStarted -= GameStarted;
        GameManager.OnGameEnd -= GameEnded;
        GameManager.OnGameReset -= GameReset;
        PauseMenuController.OnContinuePressed -= ContinuePressed;
        SettingsController.OnSFXSetting -= SFXSetting;
        //
        ms_instance = null;
    }


    private void Start()
    {
        float halfBlockSize = BlockSize * 0.5f;
        float X = BallPosition.position.x;

        GridPositions = new Dictionary<Types.GridPosition, Vector3>()
        {
            { Types.GridPosition.BottomLeft   , new Vector3(X, halfBlockSize, BlockSize) },
            { Types.GridPosition.BottomMiddle , new Vector3(X, halfBlockSize, 0f) },
            { Types.GridPosition.BottomRight  , new Vector3(X, halfBlockSize, -BlockSize) },
                                                      
            { Types.GridPosition.MiddleLeft   , new Vector3(X, BlockSize + halfBlockSize, BlockSize) },
            { Types.GridPosition.Middle       , new Vector3(X, BlockSize + halfBlockSize, 0f) },
            { Types.GridPosition.MiddleRight  , new Vector3(X, BlockSize + halfBlockSize, -BlockSize) },
                                                      
            { Types.GridPosition.UpperLeft    , new Vector3(X, 2f * BlockSize + halfBlockSize, BlockSize) },
            { Types.GridPosition.UpperMiddle  , new Vector3(X, 2f * BlockSize + halfBlockSize, 0f) },
            { Types.GridPosition.UpperRight   , new Vector3(X, 2f * BlockSize + halfBlockSize, -BlockSize) },

            { Types.GridPosition.Hidden       , transform.position },
        };

        //

        m_gridAllowedConnections = new Dictionary<Types.GridPosition, List<Types.ESwipeDirection>>()
        {
            { Types.GridPosition.BottomLeft   , new List<Types.ESwipeDirection>() { Types.ESwipeDirection.Up,		Types.ESwipeDirection.Right } },
            { Types.GridPosition.BottomMiddle , new List<Types.ESwipeDirection>() { Types.ESwipeDirection.Up,		Types.ESwipeDirection.Left	, Types.ESwipeDirection.Right } },
            { Types.GridPosition.BottomRight  , new List<Types.ESwipeDirection>() { Types.ESwipeDirection.Up,		Types.ESwipeDirection.Left } },
            { Types.GridPosition.MiddleLeft   , new List<Types.ESwipeDirection>() { Types.ESwipeDirection.Up,		Types.ESwipeDirection.Right	, Types.ESwipeDirection.Down } },
            { Types.GridPosition.Middle       , new List<Types.ESwipeDirection>() { Types.ESwipeDirection.Up,		Types.ESwipeDirection.Left	, Types.ESwipeDirection.Right		, Types.ESwipeDirection.Down } },
            { Types.GridPosition.MiddleRight  , new List<Types.ESwipeDirection>() { Types.ESwipeDirection.Up,		Types.ESwipeDirection.Left	, Types.ESwipeDirection.Down } },
            { Types.GridPosition.UpperLeft    , new List<Types.ESwipeDirection>() { Types.ESwipeDirection.Right,	Types.ESwipeDirection.Down } },
            { Types.GridPosition.UpperMiddle  , new List<Types.ESwipeDirection>() { Types.ESwipeDirection.Left,	Types.ESwipeDirection.Right	, Types.ESwipeDirection.Down } },
            { Types.GridPosition.UpperRight   , new List<Types.ESwipeDirection>() { Types.ESwipeDirection.Left,	Types.ESwipeDirection.Down } },
        };

        m_gridLinkConnections = new Dictionary<Types.GridPosition, Types.GridPosition[]>()
        {
            { Types.GridPosition.BottomLeft   , new Types.GridPosition[] { Types.GridPosition.Noof, Types.GridPosition.BottomMiddle, Types.GridPosition.MiddleLeft, Types.GridPosition.Noof } },
            { Types.GridPosition.BottomMiddle , new Types.GridPosition[] { Types.GridPosition.BottomLeft, Types.GridPosition.BottomRight, Types.GridPosition.Middle, Types.GridPosition.Noof } },
            { Types.GridPosition.BottomRight  , new Types.GridPosition[] { Types.GridPosition.BottomMiddle, Types.GridPosition.Noof, Types.GridPosition.MiddleRight, Types.GridPosition.Noof } },
            { Types.GridPosition.MiddleLeft   , new Types.GridPosition[] { Types.GridPosition.Noof, Types.GridPosition.Middle, Types.GridPosition.UpperLeft, Types.GridPosition.BottomLeft } },
            { Types.GridPosition.Middle       , new Types.GridPosition[] { Types.GridPosition.MiddleLeft, Types.GridPosition.MiddleRight, Types.GridPosition.UpperMiddle, Types.GridPosition.BottomMiddle } },
            { Types.GridPosition.MiddleRight  , new Types.GridPosition[] { Types.GridPosition.Middle, Types.GridPosition.Noof, Types.GridPosition.UpperRight, Types.GridPosition.BottomRight} },
            { Types.GridPosition.UpperLeft    , new Types.GridPosition[] { Types.GridPosition.Noof, Types.GridPosition.UpperMiddle, Types.GridPosition.Noof, Types.GridPosition.MiddleLeft } },
            { Types.GridPosition.UpperMiddle  , new Types.GridPosition[] { Types.GridPosition.UpperLeft, Types.GridPosition.UpperRight, Types.GridPosition.Noof, Types.GridPosition.Middle } },
            { Types.GridPosition.UpperRight   , new Types.GridPosition[] { Types.GridPosition.UpperMiddle, Types.GridPosition.Noof, Types.GridPosition.Noof, Types.GridPosition.MiddleRight } },
        };

        // Update volume on start
        SFXSetting();
    }


    void Update ()
    {
		float dt = Time.deltaTime * GlobalTime;

        //
        if (m_targetPos != Types.GridPosition.Noof)
        {
            m_timer += dt;

            Vector3 a = GridPositions[m_currentPos];
            Vector3 b = GridPositions[m_targetPos];
            transform.position = Vector3.Lerp(a, b, m_timer / AnimationTime);

            if (m_timer >= AnimationTime)
            {
                m_timer = 0f;
                m_currentPos = m_targetPos;
                m_targetPos = Types.GridPosition.Noof;
            }
        }

		// Update rotation
		transform.Rotate(0f, 0f, -dt * RotationSpeed, Space.Self);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Obstacle"))
        {
            --m_sessionLife;
            m_ignoreHit = true;
            SaveProfile.Instance.Crashes += 1;
            UpdateCrashAchivements();

            // Notifiy first, for estetics
            if (OnLifeUpdate != null)
                OnLifeUpdate(m_sessionLife);

            // Check for end game
            if (m_sessionLife == 0)
            {
                // Disable collisions
                GetComponent<SphereCollider>().enabled = false;

				IsNewBestScore = (m_sessionScore > SaveProfile.Instance.BestScore);

                // Notify
                GameManager.Instance.GameFinished();              
            }

            // Play sound
            m_source.Play();
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("ObstacleParent"))
        {
            if (m_ignoreHit)
            {
                m_ignoreHit = false;
                return;
            }

            ++m_sessionScore;
            UpdateScoreAchivements(m_sessionScore);

            if (OnScoreUpdate != null)
                OnScoreUpdate(m_sessionScore);
        }
    }

	#endregion


	#region Members

	public void Swipe(Types.ESwipeDirection direction)
	{
		// Forbid moving whle in motion
		if (m_targetPos != Types.GridPosition.Noof)
			return;

		if (BlockSwipe)
			return;

		//

		bool moveAllowed = m_gridAllowedConnections[m_currentPos].Contains(direction);
		if (!moveAllowed)
			return;

		m_targetPos = m_gridLinkConnections[m_currentPos][(int)direction];

		//
		SaveProfile.Instance.Swipes += 1;
		AchievementsManager.Instance.IncrementAchievement(AchievementsManager.AchvName.SwipeForest, 1);
		AchievementsManager.Instance.IncrementAchievement(AchievementsManager.AchvName.Machine, 1);
	}

	#endregion


	#region Internal

	private void UpdateScoreAchivements(int score)
    {
        AchievementsManager.Instance.UpdateAchievement(AchievementsManager.AchvName.NoSweat, score);
        AchievementsManager.Instance.UpdateAchievement(AchievementsManager.AchvName.FigerOnFire, score);
        AchievementsManager.Instance.UpdateAchievement(AchievementsManager.AchvName.CallMeSir, score);
    }
    private void UpdateCrashAchivements()
    {
        AchievementsManager.Instance.IncrementAchievement(AchievementsManager.AchvName.FirstImpact, 1);
        AchievementsManager.Instance.IncrementAchievement(AchievementsManager.AchvName.ThatHurt, 1);
        AchievementsManager.Instance.IncrementAchievement(AchievementsManager.AchvName.WhatPain, 1);
    }

    #endregion


    #region Callbacks
	
    private void GameStarted()
    {
        m_sessionScore = 0;
        m_sessionLife = SaveProfile.Instance.Lifes;
        m_timer = 0f;
        m_ignoreHit = false;
        // Enable collisions
        GetComponent<SphereCollider>().enabled = true;
        //
        // First time setting
        if (m_currentPos == Types.GridPosition.Noof)
            m_currentPos = Types.GridPosition.Hidden;
        m_targetPos = Types.GridPosition.Middle;
        //
        UpdateScoreAchivements(SaveProfile.Instance.BestScore);
        //
        AnalyticsEvent.Custom("game_started", new Dictionary<string, object>
        {
            { "life", m_sessionLife }
        });  
    }


    private void GameEnded()
    {
        // Move to middle
        m_timer = 0f;
		m_targetPos = Types.GridPosition.Noof;
		//
		StartCoroutine(GameOverCoroutine());

		// Update leaderboard
		Novulo.Social.SocialManager.Get.PublishLeaderboardEntry(GameManager.GetLeaderboardID(), m_sessionScore);

        //
        AnalyticsEvent.Custom("game_finished", new Dictionary<string, object>
        {
            { "score", m_sessionScore }
        });  
    }


    private void GameReset()
    {
        Vector3 newPos = transform.position;
        newPos.x = -400f;
        //ransform.position = newPos;
        //
        m_timer = 0f;
        m_currentPos = Types.GridPosition.Middle;
        m_targetPos = Types.GridPosition.Hidden;
    }


    private void SFXSetting()
    {
        m_source.mute = !SaveProfile.Instance.SFX;
    }


    private void ContinuePressed()
    {
        if (OnLifeUpdate != null)
            OnLifeUpdate(m_sessionLife);
    }

	#endregion


	#region Coroutine

	private IEnumerator GameOverCoroutine()
	{
		GlobalTime = 0.15f;
		yield return new WaitForSeconds(1f);
		//
		m_targetPos = Types.GridPosition.Middle;
		//
		float timer = 0f;
		float animTime = 2f;
		while (timer < animTime)
		{
			timer += Time.deltaTime;
			GlobalTime = Mathf.Lerp(0.15f, 1f, timer / animTime);
			yield return new WaitForEndOfFrame();
		}
	}

	#endregion
}

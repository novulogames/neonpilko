﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using SimpleJSON;


public class Language : MonoBehaviour 
{
	#region Events

	public delegate void LanguageLoadedCallback();
	public static LanguageLoadedCallback OnLanguageLoaded;

	#endregion


	#region Types

	public enum Lang
	{
		EO = 0,
		EN,
		PT,
        ES,
		FR,
        DE,
        IT,
        RU,

		Noof
	}

	#endregion


	#region Members

	public TextAsset TranslationFile;

	public static Dictionary<string, Lang> StringToLang = new Dictionary<string, Lang>(){
		{ "eo", Lang.EO },
		{ "en", Lang.EN },
		{ "pt", Lang.PT },
        { "es", Lang.ES },
		{ "fr", Lang.FR },
        { "de", Lang.DE },
        { "it", Lang.IT },
        { "ru", Lang.RU },
	};

	private Dictionary<string, string> m_langData = new Dictionary<string, string>();
	private static Language ms_instance = null;

	#endregion


	#region Properties

	public static Language Instance
	{
		get { return ms_instance; }
	}

	#endregion


	#region MonoBehaviour

	void Awake ()
	{
		ms_instance = this;
	}

	void OnDestroy ()
	{
		ms_instance = null;
	}

	#endregion


	#region Methods

	public void Load(Lang lang)
	{
		// In case we change language
		m_langData.Clear();

		//
		JSONNode root = JSON.Parse(TranslationFile.text);
		JSONArray array = root["translations"].AsArray;
		{
			int count = array.Count;
			string id = "";
			string txt = "";
			string langID = lang.ToString().ToLower();

			for (int idx = 0; idx < count; ++idx)
			{
				id = array[idx]["id"].Value;
				txt = array[idx][langID].Value;
				m_langData.Add(id, txt);
			}
		}

		// Notify
		if (OnLanguageLoaded != null)
			OnLanguageLoaded();
			
        //
        AnalyticsEvent.Custom("langauge_set", new Dictionary<string, object>
        {
            { "language", lang.ToString() }
        });  

		//
		if (lang == Lang.EO)
		{
			AchievementsManager.Instance.IncrementAchievement(AchievementsManager.AchvName.Saluton, 1);
		}
	}

	public string Get(string id)
	{
		if (!m_langData.ContainsKey(id))
		{
			Debug.LogError(string.Format("ID {0} is missing", id));
			return "MISSING";
		}

		return m_langData[id];
	}

	#endregion
}

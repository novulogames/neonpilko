﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    #region Members

    public AudioClip MenuClip;
    public AudioClip IngameClip;

    public float FadeSpeed;

    private AudioSource m_source;

    #endregion


    #region MonoBehaviour

    private void Awake()
    {
        GameManager.OnGameStarted += GameStarted;
        GameManager.OnGameEnd += GameEnded;
        SettingsController.OnMusicSetting += MusicSetting;
        //
        m_source = GetComponent<AudioSource>();
    }

    private void OnDestroy()
    {
        GameManager.OnGameStarted -= GameStarted;
        GameManager.OnGameEnd -= GameEnded;
        SettingsController.OnMusicSetting -= MusicSetting;
    }


    public void Start()
    {
        // Update volume on start
        MusicSetting();
        //
        m_source.clip = MenuClip;
        m_source.loop = true;
        m_source.Play();
    }
    #endregion


    #region Callbacks

    private void GameStarted()
    {
        StopAllCoroutines();
        //
        IEnumerator coroutine = SwitchToGameMusicCoroutine();
        StartCoroutine(coroutine);
    }


    private void GameEnded()
    {
        StopAllCoroutines();
        //
        IEnumerator coroutine = SwitchToMenuMusicCoroutine();
        StartCoroutine(coroutine);
    }


    private void MusicSetting()
    {
        m_source.mute = !SaveProfile.Instance.Music;
    }

    #endregion


    #region Coroutine

    private IEnumerator SwitchToGameMusicCoroutine()
    {
        do
        {
            m_source.volume -= (Time.deltaTime * FadeSpeed);
            yield return new WaitForEndOfFrame();
        }
        while (m_source.volume > 0f);

        //

        m_source.clip = IngameClip;
        m_source.loop = true;
        m_source.Play();

        do
        {
            m_source.volume += (Time.deltaTime * FadeSpeed);
            yield return new WaitForEndOfFrame();
        }
        while (m_source.volume < 1f);
    }


    private IEnumerator SwitchToMenuMusicCoroutine()
    {
        m_source.loop = false;

        do
        {
            m_source.volume -= (Time.deltaTime * FadeSpeed);
            yield return new WaitForEndOfFrame();
        }
        while (m_source.volume > 0f);

        //

        m_source.clip = MenuClip;
        m_source.loop = true;
        m_source.Play();
        
        do
        {
            m_source.volume += (Time.deltaTime * FadeSpeed);
            yield return new WaitForEndOfFrame();
        }
        while (m_source.volume < 1f);
    }

    #endregion
}

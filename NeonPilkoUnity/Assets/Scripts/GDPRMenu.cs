﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDPRMenu : MonoBehaviour
{
	#region Callbacks

	public void ReadPressed()
	{
		Application.OpenURL("https://novulo.games/privacy-policy/");
	}

	public void AcceptPressed()
	{
		SaveProfile.Instance.GDPRAccepted = true;
		Novulo.Adverts.AdsManager.Get.GiveGDPRConcent(SaveProfile.Instance.GDPRAccepted);

		//

		if (SaveProfile.Instance.GameLanguage == Language.Lang.Noof)
		{
			SceneLoader.LoadLanguageScene();
		}
		else
		{
			Language.Instance.Load(SaveProfile.Instance.GameLanguage);
			SceneLoader.LoadMainScene();
		}
	}

	#endregion
}

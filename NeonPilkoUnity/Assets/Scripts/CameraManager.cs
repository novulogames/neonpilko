﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    #region Members

    public Transform TargetTransform;
    public float Distance;
    public float CatchupSpeed;

    #endregion


    #region MonoBehaviour
    
    void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
        Camera.main.transform.position = TargetTransform.position - new Vector3(Distance, 0f, 0f);
    }

    #endregion
}

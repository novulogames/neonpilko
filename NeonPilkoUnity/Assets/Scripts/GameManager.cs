﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;


public class GameManager : MonoBehaviour
{
    #region Events

    public delegate void GameInitCallback();
    public static GameInitCallback OnGameInit;

    public delegate void GameStartedCallback();
    public static GameStartedCallback OnGameStarted;

    public delegate void GameEndCallback();
    public static GameEndCallback OnGameEnd;

    public delegate void GameRestartCallback();
    public static GameRestartCallback OnGameRestart;

    public delegate void GameResetCallback();
    public static GameResetCallback OnGameReset;

    #endregion


    #region Members

    private static GameManager ms_instance = null;

    #endregion


    #region Properties

    public static GameManager Instance
    {
        get { return ms_instance; }
    }

    #endregion


    #region MonoBehaviour

    private void Awake()
    {
        ms_instance = this;
        //
        MainMenuController.OnPlayPressed += StartGame;
		Novulo.Store.IAPManager.OnIAPPurchaseComplete += IAPComplete;
	}


    void Start()
    {
		Application.targetFrameRate = 60;
		//
        IEnumerator coroutine = GameInit();
        StartCoroutine(coroutine);
    }


    private void OnDestroy()
    {
        ms_instance = null;
        //
        MainMenuController.OnPlayPressed -= StartGame;
		Novulo.Store.IAPManager.OnIAPPurchaseComplete -= IAPComplete;
	}
    
    #endregion


    #region Methods

    public void GameFinished()
    {
        if (OnGameEnd != null)
            OnGameEnd();
    }


    public void GameRestart()
    {
        if (OnGameRestart != null)
            OnGameRestart();

		StartGame();
    }


    public void GameReset()
    {
		if (OnGameReset != null)
            OnGameReset();
    }

	public static string GetLeaderboardID()
	{
		string id = string.Empty;

		if (SaveProfile.Instance.LevelMode == Types.ELevelMode.Slow)
		{
			id = GPGSIds.leaderboard_slow;
		}
		else
		{
			if (SaveProfile.Instance.MimeMode)
				id = GPGSIds.leaderboard_mime;
			else
				id = GPGSIds.leaderboard_fast;
		}

		return id;
	}

	public void ProcessIAP(string IAPName, bool success)
	{
		if (!success)
			return;

		//

		if (string.Equals(IAPName, "neonball.removeads"))
			SaveProfile.Instance.RemoveAds = true;
		else if (string.Equals(IAPName, "neonball.fullhealth"))
			SaveProfile.Instance.FullHealth = true;
	}

	#endregion


	#region Callbacks

	private void StartGame()
    {
        if (OnGameStarted != null)
            OnGameStarted();

        // Increment counter for playing video ads
        // between plays
        ++SaveProfile.Instance.VideoCounter;

        //
        SaveProfile.Instance.SessionsPlayed += 1;
        AchievementsManager.Instance.IncrementAchievement(AchievementsManager.AchvName.GameGood, 1);
    }

	private void IAPComplete(string IAPName, bool success)
	{
		ProcessIAP(IAPName, success);
	}

    private void AdFinishedCallback(Novulo.Adverts.AdsManager.AdResult result)
    {
        IEnumerator coroutine = AdReturnCoroutine();
        StartCoroutine(coroutine);
    }

    #endregion


    #region Coroutine

    private IEnumerator GameInit()
    {
        SaveProfile.Instance.LoadData();
		AchievementsManager.Instance.LoadData();
		//
		Novulo.Store.IAPManager.Get.Init();
		yield return new WaitForSeconds(1f);

		//

		if (OnGameInit != null)
            OnGameInit();

        yield return new WaitForSeconds(2f);

        //
		if (!SaveProfile.Instance.GDPRAccepted)
		{
			SceneLoader.LoadGDPR();
		}
        else
		{
			Novulo.Adverts.AdsManager.Get.GiveGDPRConcent(SaveProfile.Instance.GDPRAccepted);
			if (SaveProfile.Instance.GameLanguage == Language.Lang.Noof)
			{
				SceneLoader.LoadLanguageScene();
			}
			else
			{
				Language.Instance.Load(SaveProfile.Instance.GameLanguage);
				SceneLoader.LoadMainScene();
			}
        }
    }

    private IEnumerator AdReturnCoroutine()
    {
        yield return new WaitForSeconds(0.25f);

        // Go normal path
        StartGame();
    }

    #endregion
}

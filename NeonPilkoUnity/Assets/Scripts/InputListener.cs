﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputListener : MonoBehaviour
{
    #region Members

    private Vector2 m_startPos;
    private Vector2 m_endPos;
	private bool m_hasSwiped = false;
    private float m_threshold = 40f;

    Vector2[] m_swipeArray = new Vector2[4];

    #endregion


    #region MonoBehaviour
    
    void Awake ()
    {
        GameManager.OnGameStarted += GameStarted;
        GameManager.OnGameEnd += GameEnded;
		InGameMenuController.OnPausePressed += PausePressed;
        PauseMenuController.OnContinuePressed += ContinuePressed; 
        //
        enabled = false;
	}


    void Start ()
    {
        m_swipeArray[(int)Types.ESwipeDirection.Left] = Vector2.left;
        m_swipeArray[(int)Types.ESwipeDirection.Right] = Vector2.right;
        m_swipeArray[(int)Types.ESwipeDirection.Up]   = Vector2.up;
        m_swipeArray[(int)Types.ESwipeDirection.Down] = Vector2.down;
    }

    private void OnDestroy()
    {
        GameManager.OnGameStarted -= GameStarted;
        GameManager.OnGameEnd -= GameEnded;
		InGameMenuController.OnPausePressed -= PausePressed;
        PauseMenuController.OnContinuePressed -= ContinuePressed; 
    }


    void Update ()
    {
#if UNITY_EDITOR

		if (Input.GetMouseButtonDown(0))
		{
			m_hasSwiped = false;
			m_startPos = Input.mousePosition;
		}
		else if (Input.GetMouseButton(0) && !m_hasSwiped)
		{
			m_endPos = Input.mousePosition;
			float sqrMag = (m_endPos - m_startPos).sqrMagnitude;
			if (sqrMag > 2000)
				DoSwipe();
		}
        else if (Input.GetMouseButtonUp(0) && !m_hasSwiped)
        {
            m_endPos = Input.mousePosition;
			DoSwipe();
        }
#else

        if (Input.touchCount != 0)
        {
            Touch t = Input.touches[0];
            switch (t.phase)
            {
                case TouchPhase.Began:
                    {
						m_hasSwiped = false;
                        m_startPos = t.position;
                    }
                    break;

				case TouchPhase.Moved:
					{
						if (m_hasSwiped)
							return;

						m_endPos = Input.mousePosition;
						float sqrMag = (m_endPos - m_startPos).sqrMagnitude;
						if (sqrMag > 1000)
						{
							DoSwipe();
						}
					}
					break;
				
                case TouchPhase.Ended:
                    {
						if (m_hasSwiped)
							return;

                        m_endPos = t.position;
						DoSwipe();
                    }
                    break;

                default:
                    break;
            }
        }

#endif

		// Allow ArrowKey for the lucky
		if (Input.GetKeyUp(KeyCode.LeftArrow))
		{
			BallController.Instance.Swipe(Types.ESwipeDirection.Left);
		}
		else if (Input.GetKeyUp(KeyCode.RightArrow))
		{
			BallController.Instance.Swipe(Types.ESwipeDirection.Right);
		}
		else if (Input.GetKeyUp(KeyCode.UpArrow))
		{
			BallController.Instance.Swipe(Types.ESwipeDirection.Up);
		}
		else if (Input.GetKeyUp(KeyCode.DownArrow))
		{
			BallController.Instance.Swipe(Types.ESwipeDirection.Down);
		}
	}

	#endregion


	#region Internal

	private void DoSwipe()
	{
		Vector2 dir = m_endPos - m_startPos;
		float angle = 0f;

		for (int idx = 0; idx < (int)Types.ESwipeDirection.Noof; ++idx)
		{
			angle = Mathf.Abs(Vector2.Angle(dir, m_swipeArray[idx]));
			if (angle <= m_threshold)
			{
				BallController.Instance.Swipe((Types.ESwipeDirection)idx);
				break;
			}
		}

		m_startPos = Vector2.zero;
		m_endPos = Vector2.zero;
		m_hasSwiped = true;
	}

	#endregion


	#region Callbacks

	private void GameStarted()
    {
        m_startPos = Vector2.zero;
        m_endPos = Vector2.zero;
        //
        enabled = true;
    }

    private void GameEnded()
    {
        enabled = false;
    }

    private void PausePressed()
    {
        enabled = false;
    }

    private void ContinuePressed()
    {
        m_startPos = Vector2.zero;
        m_endPos = Vector2.zero;
        //
        enabled = true;
    }

    #endregion
}

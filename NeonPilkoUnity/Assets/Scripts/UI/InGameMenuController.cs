﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;


public class InGameMenuController : MonoBehaviour, Novulo.Platform.Common.IBackButtonListener
{
    #region Events

    public delegate void PausePressedCallback();
    public static PausePressedCallback OnPausePressed;

    #endregion


    #region Members

    public GameObject Group;
    public Text ScoreText;
	[Space]
    public Transform[] Hearts;
	[Space]
	public GameObject FastLabel;
	public GameObject MimeLabel;

    private LocalizedText m_locText;

    #endregion


    #region MonoBehaviour

    void Awake ()
    {
        GameManager.OnGameStarted += GameStarted;
        GameManager.OnGameEnd += GameEnded;
        BallController.OnScoreUpdate += UpdateScore;
        BallController.OnLifeUpdate += UpdateLife;
        PauseMenuController.OnContinuePressed += ContinuePressed;
        //
        m_locText = ScoreText.transform.GetComponent<LocalizedText>();
        UpdateScore(0);
	}


    private void OnDestroy()
    {
        GameManager.OnGameStarted -= GameStarted;
        GameManager.OnGameEnd -= GameEnded;
        BallController.OnScoreUpdate -= UpdateScore;
        BallController.OnLifeUpdate -= UpdateLife;
        PauseMenuController.OnContinuePressed -= ContinuePressed;
        //
        StopAllCoroutines();
    }

	#endregion


	#region IBackButtonListener

	public void HandleBackButton()
	{
		PausePressed();
	}

	#endregion


	#region Callbacks

	private void GameStarted()
	{
		Novulo.Platform.Common.BackButtonHandler.Get.RegisterListener(this);
		//
		FastLabel.SetActive(SaveProfile.Instance.LevelMode == Types.ELevelMode.Fast);
		MimeLabel.SetActive(SaveProfile.Instance.MimeMode);
		//
		Group.SetActive(true);
		//
        UpdateScore(0);
        UpdateLife(SaveProfile.Instance.Lifes);
    }


    private void GameEnded()
	{
		Novulo.Platform.Common.BackButtonHandler.Get?.UnregisterListener(this);
		Group.SetActive(false);
    }


    private void UpdateScore(int score)
    {
        string scoreText = Language.Instance.Get(m_locText.ID);
        ScoreText.text = string.Format(scoreText, score);
    }


    private void UpdateLife(int remaining)
    {
        Color active = new Color32(255, 0, 87, 255);
        Color inactive = new Color32(106, 106, 106, 255);

        for (int i = 0; i < 3; ++i)
        {
            Hearts[i].GetComponent<Animator>().SetTrigger((i < remaining) ? "pulse" : "dry");
            Hearts[i].GetComponent<Image>().color = (i < remaining) ? active : inactive;
        }
    }

    public void PausePressed()
    {
        Group.SetActive(false);
        //
        Time.timeScale = 0f;
        //
        if (OnPausePressed != null)
            OnPausePressed();
            
        //
        AnalyticsEvent.Custom("pause_pressed", new Dictionary<string, object>
        {
            { "score", BallController.Instance.SessionScore }
        });
    }


    private void ContinuePressed()
    {
        Group.SetActive(true);
        //
        Time.timeScale = 1f;
    }

    #endregion
}

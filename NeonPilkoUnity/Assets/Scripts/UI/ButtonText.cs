﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonText : MonoBehaviour
{
	#region Members

	private Button m_parent;
	private Text m_text;
	private float m_alpha;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		m_parent = GetComponentInParent<Button>();
		m_text = GetComponent<Text>();
		m_alpha = m_text.color.a;
	}

	private void Update()
	{
		Color c = m_text.color;
		if (m_parent.interactable)
		{
			c.a = m_alpha;
			m_text.color = c;
		}
		else
		{
			c.a = m_parent.colors.disabledColor.a;
		}

		m_text.color = c;
	}

	#endregion
}
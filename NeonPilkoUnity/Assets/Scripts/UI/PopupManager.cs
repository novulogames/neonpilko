﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : MonoBehaviour 
{
	#region Members

	public GameObject LifePopup;
	public GameObject SettingsPopup;
	public GameObject SupportPopup;
	public GameObject FailedPopup;
	public GameObject SkippedPopup;
	public GameObject CreditsPopup;
	public GameObject StorePopup;
	[Space]
	public GameObject Panel;

	#endregion


	#region Properties

	public static PopupManager Get { get; private set; } = null;

	#endregion


	#region MonoBehaviour

	void Awake () 
	{
		Get = this;	
	}

	void OnDestroy ()
	{
		Get = null;
	}

	#endregion


	#region Methods

	public void EnablePopup(Types.EPopupTypes popup, bool enabled)
	{
		switch (popup)
		{
			case Types.EPopupTypes.Life: LifePopup.SetActive(enabled); break;
			case Types.EPopupTypes.AdFailed: FailedPopup.SetActive(enabled); break;
			case Types.EPopupTypes.AdSkip: SkippedPopup.SetActive(enabled); break;
			case Types.EPopupTypes.Settings: SettingsPopup.SetActive(enabled); break;
			case Types.EPopupTypes.Credits: CreditsPopup.SetActive(enabled); break;
			case Types.EPopupTypes.Support: SupportPopup.SetActive(enabled); break;
			case Types.EPopupTypes.Store: StorePopup.SetActive(enabled); break;
		}

		Panel.SetActive(enabled);
	}

	public bool IsPopupEnabled(Types.EPopupTypes popup)
	{
		switch (popup)
		{
			case Types.EPopupTypes.Life: return LifePopup.activeSelf;
			case Types.EPopupTypes.AdFailed: return FailedPopup.activeSelf;
			case Types.EPopupTypes.AdSkip: return SkippedPopup.activeSelf;
			case Types.EPopupTypes.Settings: return SettingsPopup.activeSelf;
			case Types.EPopupTypes.Credits: return CreditsPopup.activeSelf;
			case Types.EPopupTypes.Support: return SupportPopup.activeSelf;
			case Types.EPopupTypes.Store: return StorePopup.activeSelf;
		}

		return false;
	}

	#endregion


	#region Callbacks

	public void CloseAllPopups()
	{
		LifePopup.SetActive(false);
		FailedPopup.SetActive(false);
		SkippedPopup.SetActive(false);
		SettingsPopup.SetActive(false);
		CreditsPopup.SetActive(false);
		SupportPopup.SetActive(false);
		StorePopup.SetActive(false);
		//
		Panel.SetActive(false);
	}

	#endregion
}

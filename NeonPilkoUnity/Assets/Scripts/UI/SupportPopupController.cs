﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;


public class SupportPopupController : MonoBehaviour, Novulo.Platform.Common.IBackButtonListener
{
	#region Members

	public Button SupportButton;

	#endregion


	#region MonoBehaviour

	void OnEnable () 
	{
		SupportButton.interactable = false;
		Novulo.Platform.Common.BackButtonHandler.Get.RegisterListener(this);
		//
		IEnumerator coroutine = CheckAdStateCoroutine();
		StartCoroutine(coroutine);
	}
	
	void OnDisable ()
	{
		Novulo.Platform.Common.BackButtonHandler.Get?.UnregisterListener(this);
		StopAllCoroutines();
		SupportButton.interactable = false;
	}

	#endregion


	#region IBackButtonListener

	public void HandleBackButton()
	{
		PopupManager.Get.EnablePopup(Types.EPopupTypes.Support, false);
	}

	#endregion


	#region Callbacks

	public void RatePressed()
	{
	#if UNITY_ANDROID
		Application.OpenURL("https://play.google.com/store/apps/details?id=eu.novulogames.neonball");
	#else
	#endif
		//
        AnalyticsEvent.Custom("rate_pressed", new Dictionary<string, object>
        {
            { "screen", "support_popup" }
        });  
	}

	public void SupportPressed()
	{
		if (!SupportButton.interactable)
			return;

		if (Novulo.Adverts.AdsManager.Get.IsAdReady(Novulo.Adverts.AdsManager.AdType.Reward))
		{
			Novulo.Adverts.AdsManager.Get.ShowAd(Novulo.Adverts.AdsManager.AdType.Reward, null, null);
		}
		else if (Novulo.Adverts.AdsManager.Get.IsAdReady(Novulo.Adverts.AdsManager.AdType.Video))
		{
			Novulo.Adverts.AdsManager.Get.ShowAd(Novulo.Adverts.AdsManager.AdType.Video, null, null);
		}
		
		//
        AnalyticsEvent.Custom("support_pressed", new Dictionary<string, object>
        {
            { "screen", "support_popup" }
        });  
	}

	#endregion

	#region Coroutine
	
	private IEnumerator CheckAdStateCoroutine()
	{
		bool isReady = true;
		do 
		{
			yield return new WaitForSeconds(0.1f);
			isReady = Novulo.Adverts.AdsManager.Get.IsAdReady(Novulo.Adverts.AdsManager.AdType.Reward);
			isReady |= Novulo.Adverts.AdsManager.Get.IsAdReady(Novulo.Adverts.AdsManager.AdType.Video);
		} while (!isReady);

		//
		SupportButton.interactable = true;
	}

	#endregion
}

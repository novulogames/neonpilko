﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsPopupController : MonoBehaviour, Novulo.Platform.Common.IBackButtonListener
{
	#region MonoBehaviour

	private void OnEnable()
	{
		Novulo.Platform.Common.BackButtonHandler.Get.RegisterListener(this);
	}

	private void OnDisable()
	{
		Novulo.Platform.Common.BackButtonHandler.Get?.UnregisterListener(this);
	}

	#endregion


	#region IBackButtonListener

	public void HandleBackButton()
	{
		gameObject.SetActive(false);
	}

	#endregion
}

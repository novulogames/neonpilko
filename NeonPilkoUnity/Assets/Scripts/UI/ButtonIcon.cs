﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonIcon : MonoBehaviour
{
	#region Members

	private Button m_parent;
	private Image m_image;
	private float m_alpha;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		m_parent = GetComponentInParent<Button>();
		m_image = GetComponent<Image>();
		m_alpha = m_image.color.a;
	}

	private void Update()
	{
		Color c = m_image.color;
		if (m_parent.interactable)
		{
			c.a = m_alpha;
			m_image.color = c;
		}
		else
		{
			c.a = m_parent.colors.disabledColor.a;
		}

		m_image.color = c;
	}

	#endregion
}

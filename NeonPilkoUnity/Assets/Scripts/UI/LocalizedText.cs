﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LocalizedText : MonoBehaviour
{
	#region Members

	public string ID = "";
	private Text m_text = null;

	#endregion


	#region MooBehaviour

	void Awake () 
	{
		Debug.Assert(!string.IsNullOrEmpty(ID));
		//
		Language.OnLanguageLoaded += UpdateText;
		//
		m_text = GetComponent<Text>();
		UpdateText();
	}
	
	void OnDestroy ()
	{
		Language.OnLanguageLoaded -= UpdateText;
	}

	#endregion


	#region Callbacks

	private void UpdateText()
	{
		if (m_text == null)
			return;

		m_text.text = Language.Instance.Get(ID);
	}

	#endregion
}

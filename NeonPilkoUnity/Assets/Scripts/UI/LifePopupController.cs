﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;


public class LifePopupController : MonoBehaviour, Novulo.Platform.Common.IBackButtonListener
{
	#region Members

	public GameObject ExtraPopup;
	public GameObject ExtendPopup;

	public Button WatchButton;
	public Text ExtendTimeText;
	public Image[] HeartImages;

	#endregion


	#region MonoBehaviour

	void OnEnable () 
	{
		Novulo.Platform.Common.BackButtonHandler.Get.RegisterListener(this);
		WatchButton.interactable = false;
		//
		IEnumerator coroutine = CheckAdStateCoroutine();
		StartCoroutine(coroutine);

		coroutine = UpdateTimerCoroutine();
		StartCoroutine(coroutine);

		//
		UpdatePopup();
	}
	
	void OnDisable ()
	{
		Novulo.Platform.Common.BackButtonHandler.Get?.UnregisterListener(this);
		StopAllCoroutines();
		WatchButton.interactable = false;
	}

	#endregion


	#region IBackButtonListener

	public void HandleBackButton()
	{
		if (PopupManager.Get.IsPopupEnabled(Types.EPopupTypes.AdFailed))
			PopupManager.Get.EnablePopup(Types.EPopupTypes.AdFailed, false);
		else if (PopupManager.Get.IsPopupEnabled(Types.EPopupTypes.AdSkip))
			PopupManager.Get.EnablePopup(Types.EPopupTypes.AdSkip, false);
		else
			CancelPressed();
	}
	#endregion


	#region Internal

	private void UpdatePopup()
	{
		ExtraPopup.SetActive(SaveProfile.Instance.Lifes < SaveProfile.MaxLifes);
		ExtendPopup.SetActive(SaveProfile.Instance.Lifes == SaveProfile.MaxLifes);

		//
        Color active = new Color32(255, 0, 87, 255);
        Color inactive = new Color32(106, 106, 106, 255);
		int lifes = SaveProfile.Instance.Lifes;

        for (int i = 0; i < SaveProfile.MaxLifes; ++i)
        {
            HeartImages[i].GetComponent<Animator>().SetTrigger((i < lifes) ? "pulse" : "dry");
            HeartImages[i].GetComponent<Image>().color = (i < lifes) ? active : inactive;
        }
	}

	#endregion


	#region Coroutine

	private IEnumerator CheckAdStateCoroutine()
	{
		bool isReady = true;
		do 
		{
			yield return new WaitForSeconds(0.1f);
			isReady = Novulo.Adverts.AdsManager.Get.IsAdReady(Novulo.Adverts.AdsManager.AdType.Reward);
		} while (!isReady);

		//
		WatchButton.interactable = true;
	}

	private IEnumerator UpdateTimerCoroutine()
	{
		while (true)
		{			
			System.TimeSpan ts = new System.TimeSpan(0, 0, (int)SaveProfile.Instance.ExtraLifeEndTime);
			ExtendTimeText.text = ts.ToString();

			yield return new WaitForSeconds(1f);
		}
	}

	#endregion


	#region Callbacks

	public void WatchPressed()
	{
		if (!WatchButton.interactable)
			return;

		//
		string popup = (ExtraPopup.activeSelf) ? "extra_popup" : "extend_popup";
        AnalyticsEvent.Custom("ad_request", new Dictionary<string, object>
        {
            { "screen", popup }
        });

		//
		Novulo.Adverts.AdsManager.Get.ShowAd(Novulo.Adverts.AdsManager.AdType.Reward, null, AdFinishedCallback);
	}

	public void CancelPressed()
	{
		PopupManager.Get.CloseAllPopups();

		string popup = (ExtraPopup.activeSelf) ? "extra_popup" : "extend_popup";
		AnalyticsEvent.Custom("life_ad_cancel", new Dictionary<string, object>
        {
            { "screen", popup }
        }); 
	}


	private void AdFinishedCallback(Novulo.Adverts.AdsManager.AdResult result)
	{
		//
		switch(result)
		{
			case Novulo.Adverts.AdsManager.AdResult.Finished:
			{
				int lifes = SaveProfile.Instance.Lifes + 1;
				if (lifes <= SaveProfile.MaxLifes)
				{
					// Set extra life time to 10 minutes
					SaveProfile.Instance.ExtraLifeEndTime = 600f;
				}
				else
				{
					// Add extra 10 minutes to life time
					SaveProfile.Instance.ExtraLifeEndTime += 600f;
				}
				
				// Add extra life
				++SaveProfile.Instance.Lifes;
			}
			break;

			case Novulo.Adverts.AdsManager.AdResult.Failed	: PopupManager.Get.EnablePopup(Types.EPopupTypes.AdFailed, true); break;
			case Novulo.Adverts.AdsManager.AdResult.Skipped: PopupManager.Get.EnablePopup(Types.EPopupTypes.AdSkip, true);	break;
		}

		//
		UpdatePopup();
	}

	#endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleController : MonoBehaviour 
{
	#region Members

	private int m_counter = 0;

	#endregion
	
	
	#region Callback

	public void TitlePressed ()
	{
		++m_counter;
		if (m_counter >= 10)
		{
			AchievementsManager.Instance.UpdateAchievement(AchievementsManager.AchvName.TitleSmasher, 1);
		}
	}

	#endregion
}

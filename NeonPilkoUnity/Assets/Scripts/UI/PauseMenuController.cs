﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;


public class PauseMenuController : MonoBehaviour, Novulo.Platform.Common.IBackButtonListener
{
	#region Events

	public delegate void ContinuePressedCallback();
	public static ContinuePressedCallback OnContinuePressed;

	#endregion


	#region Members

	public GameObject Group;

	#endregion


	#region MonoBehaviour

	void Awake () 
	{
		InGameMenuController.OnPausePressed += PausePressed;
	}

	void OnDestroy ()
	{
		InGameMenuController.OnPausePressed -= PausePressed;
	}

	#endregion


	#region IBackButtonListener

	public void HandleBackButton()
	{
		ContinuePressed();
	}

	#endregion


	#region Callbacks

	private void PausePressed()
	{
		Novulo.Platform.Common.BackButtonHandler.Get.RegisterListener(this);
		Group.SetActive(true);
	}

	public void ContinuePressed()
	{
		Novulo.Platform.Common.BackButtonHandler.Get?.UnregisterListener(this);
		Group.SetActive(false);

		if (OnContinuePressed != null)
			OnContinuePressed();

        //
        AnalyticsEvent.Custom("continue_pressed", new Dictionary<string, object>());  
	}

	public void QuitPressed()
	{
		Novulo.Platform.Common.BackButtonHandler.Get?.UnregisterListener(this);
		Group.SetActive(false);
        GameManager.Instance.GameFinished();
        //GameManager.Instance.GameReset();
		//
		Time.timeScale = 1f;

        //
        AnalyticsEvent.Custom("quit_pressed", new Dictionary<string, object>()
        {
            { "score", BallController.Instance.SessionScore }
        });  
	}

	#endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageController : MonoBehaviour 
{
	#region Members

	public GameObject LanguagesGroup;
	public GameObject TitleGroup;

	#endregion


	#region Callbacks

	public void LanguageSelected(string name)
	{
#if DEBUG
		if (!Language.StringToLang.ContainsKey(name))
		{
			Debug.Assert(false, "language not found");
		}
		else
#endif
		{
			// Switch game objects
			LanguagesGroup.SetActive(false);
			TitleGroup.SetActive(true);

			// Load language strings
			Language.Lang lang = Language.StringToLang[name];
			Language.Instance.Load(lang);

			// Save in profile
			SaveProfile.Instance.GameLanguage = lang;

			// Load main scene
			SceneLoader.LoadMainScene();
		}
	}

	#endregion
}

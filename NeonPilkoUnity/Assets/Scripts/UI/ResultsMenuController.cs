﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;


public class ResultsMenuController : MonoBehaviour, Novulo.Platform.Common.IBackButtonListener
{
    #region Events

    public delegate void ReplayPressedCallback();
    public static ReplayPressedCallback OnReplayPressed;

    #endregion


    #region Members

    public GameObject Group;
	public GameObject StarsGroup;
    public Text ScoreText;
    public Button RetryButton;
    public GameObject LifePopup;

    private LocalizedText m_locText;

    #endregion


    #region MonoBehaviour

    private void Awake()
    {
        GameManager.OnGameEnd += GameEnded;
        //
        m_locText = ScoreText.transform.GetComponent<LocalizedText>();
    }


    private void OnDestroy()
    {
        GameManager.OnGameEnd -= GameEnded;
    }

	#endregion


	#region IBackButtonListener

	public void HandleBackButton()
	{
		MenuPressed();
	}

	#endregion


	#region Callbacks

	private void GameEnded()
    {
		StartCoroutine(GameOverCoroutine());
	}
    

    public void RetryPressed()
    {
        Group.SetActive(false);
        GameManager.Instance.GameRestart();

        //
        AnalyticsEvent.Custom("retry_pressed", new Dictionary<string, object>()
        {
            { "score", BallController.Instance.SessionScore }
        });  
    }


    public void MenuPressed()
	{
		Novulo.Platform.Common.BackButtonHandler.Get?.UnregisterListener(this);
		Group.SetActive(false);
        GameManager.Instance.GameReset();

        //
        AnalyticsEvent.Custom("quit_pressed", new Dictionary<string, object>
        {
            { "score", BallController.Instance.SessionScore }
        });  
    }

    public void ExtraPressed()
    {
        LifePopup.SetActive(true);

        //
        AnalyticsEvent.Custom("extra_pressed", new Dictionary<string, object>
        {
            { "lifes", SaveProfile.Instance.Lifes },
            { "time", SaveProfile.Instance.ExtraLifeEndTime }
        });
    }

	#endregion


	#region Coroutine

	private IEnumerator GameOverCoroutine()
	{
		bool showVideo = true;
		showVideo &= (SaveProfile.Instance.VideoCounter % SaveProfile.MaxVideoCount == 0);
		showVideo &= Novulo.Adverts.AdsManager.Get.IsAdReady(Novulo.Adverts.AdsManager.AdType.Video);
		showVideo &= (SaveProfile.Instance.RemoveAds == false);
		if (showVideo)
		{
			yield return new WaitForSeconds(1f);
			//
			AnalyticsEvent.Custom("ad_request", new Dictionary<string, object>
			{
				{ "screen", "result_menu" }
			});

			// Reset evil counter
			SaveProfile.Instance.VideoCounter = 0;
			// Show ad
			Novulo.Adverts.AdsManager.Get.ShowAd(Novulo.Adverts.AdsManager.AdType.Video, null, null);
		}
		//
		yield return new WaitForSeconds(1f);
		//
		Group.SetActive(true);
		//
		string scoreText = Language.Instance.Get(m_locText.ID);
		string colorName = string.Empty;
		if (BallController.Instance.IsNewBestScore)
			colorName = "#e0bd19"; // Gold
		else if (BallController.Instance.SessionScore == SaveProfile.Instance.BestScore)
			colorName = "green";
		else
			colorName = "red";

		StarsGroup.SetActive(BallController.Instance.IsNewBestScore);
		ScoreText.text = string.Format(scoreText, BallController.Instance.SessionScore, SaveProfile.Instance.BestScore, colorName);

		//
		SaveProfile.Instance.BestScore = Mathf.Max(BallController.Instance.SessionScore, SaveProfile.Instance.BestScore);
		//
		yield return new WaitForSeconds(1f);
		Novulo.Platform.Common.BackButtonHandler.Get.RegisterListener(this);
	}

	#endregion
}

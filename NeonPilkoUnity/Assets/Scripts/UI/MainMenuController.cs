﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;


public class MainMenuController : MonoBehaviour, Novulo.Platform.Common.IBackButtonListener
{
    #region Events

    public delegate void PlayPressedCallback();
    public static PlayPressedCallback OnPlayPressed;

    #endregion


    #region Members

    public GameObject Group;
    public Button PlayButton;
    public GameObject SocialButton;
	[Space]
	public GameObject GameModeGroup;
	public GameObject MimeMode;
	public Slider GameModeSlider;
	public Toggle MimeModeToggle;
	[Space]
	public GameObject StoreButton;
	public GameObject HealthButton;

	#endregion


    #region MonoBehaviour

    private void Awake()
	{
		Novulo.Store.IAPManager.OnIAPPurchaseComplete += IAPComplete;
		Novulo.Store.IAPManager.OnStoreInit += StoreInit;
		GameManager.OnGameStarted += GameStarted;
        GameManager.OnGameReset += GameReset;
    }

    private void OnDestroy()
	{
		Novulo.Store.IAPManager.OnIAPPurchaseComplete -= IAPComplete;
		Novulo.Store.IAPManager.OnStoreInit -= StoreInit;
		GameManager.OnGameStarted -= GameStarted;
        GameManager.OnGameReset -= GameReset;
    }

	private void Start()
	{
		GameReset();
		StartCoroutine(BannerCoroutine());

		//
#if UNITY_ANDROID
		string arch = "NotDef";
		for (; ; )
		{
			if (!string.IsNullOrEmpty(SystemInfo.operatingSystem))
			{
				if ((SystemInfo.operatingSystem.IndexOf("32bit") != -1)		||
					(SystemInfo.operatingSystem.IndexOf("32 bit") != -1)	)
				{
					arch = "32";
					break;
				}
				else if ((SystemInfo.operatingSystem.IndexOf("64bit") != -1) ||
					(SystemInfo.operatingSystem.IndexOf("64 bit") != -1))
				{
					arch = "64";
					break;
				}
			}

			if (!string.IsNullOrEmpty(SystemInfo.processorType))
			{
				if (SystemInfo.processorType.IndexOf("ARM64") != -1)
				{
					arch = "64";
					break;
				}
				else
				{
					arch = "32";
					break;
				}
			}

			break;
		}

		AnalyticsEvent.Custom("cpu_arch", new Dictionary<string, object>{
				{ "arch", arch }
		});
#endif
	}

	private void OnEnable()
	{
		if (!Novulo.Store.IAPManager.Get.IsInitialised)
			Novulo.Store.IAPManager.Get.Init();

		Novulo.Platform.Common.BackButtonHandler.Get.RegisterListener(this);
	}

	private void OnDisable()
	{
		Novulo.Platform.Common.BackButtonHandler.Get?.UnregisterListener(this);
	}

	#endregion


	#region IBackButtonListener

	public void HandleBackButton()
	{
		Application.Quit();
	}

	#endregion


	#region Internal

	private void UpdateStoreButtons()
	{
		StoreButton.GetComponent<Button>().interactable = Novulo.Store.IAPManager.Get.IsInitialised;
		StoreButton.SetActive(SaveProfile.Instance.RemoveAds == false || SaveProfile.Instance.FullHealth == false);
		HealthButton.SetActive(SaveProfile.Instance.FullHealth == false);
	}

	#endregion


	#region Callbacks

	public void PlayPressed()
    {
        PlayButton.interactable = false;

        if (OnPlayPressed != null)
            OnPlayPressed();
    }

    public void SettingsPressed()
    {
        PopupManager.Get.EnablePopup(Types.EPopupTypes.Settings,true);
        
        //
        AnalyticsEvent.Custom("settings_pressed", new Dictionary<string, object>());  
    }

    public void ExtraPressed()
    {
		PopupManager.Get.EnablePopup(Types.EPopupTypes.Life, true);

		//
		AnalyticsEvent.Custom("extra_pressed", new Dictionary<string, object>
        {
            { "lifes", SaveProfile.Instance.Lifes },
            { "time", SaveProfile.Instance.ExtraLifeEndTime }
        });  
    }

	public void StorePressed()
	{
		if (!StoreButton.GetComponent<Button>().interactable)
			return;

		PopupManager.Get.EnablePopup(Types.EPopupTypes.Store, true);
	}

    public void SupportPressed()
    {
		PopupManager.Get.EnablePopup(Types.EPopupTypes.Support, true);

		//
		AnalyticsEvent.Custom("support_pressed", new Dictionary<string, object>());
    }

    public void LeaderboardPressed()
	{
		AnalyticsEvent.Custom("leaderboard_pressed", new Dictionary<string, object>());
		//
		if (!Novulo.Social.SocialManager.Get.IsLoggedIn)
		{
			GooglePlayPressed();
			return;
		}

		//
		Novulo.Social.SocialManager.Get.ShowLeaderboardsUI();
    }

    public void AchievementsPressed()
	{
		AnalyticsEvent.Custom("achievement_pressed", new Dictionary<string, object>());
		//
		if (!Novulo.Social.SocialManager.Get.IsLoggedIn)
		{
			GooglePlayPressed();
			return;
		}

		//
		Novulo.Social.SocialManager.Get.ShowAchievementsUI();
    }

    public void GooglePlayPressed()
	{
		AnalyticsEvent.Custom("googleplay_pressed", new Dictionary<string, object>());
		//
		if (!Novulo.Social.SocialManager.Get.IsLoggedIn)
			Novulo.Social.SocialManager.Get.LogIn();
    }


    private void GameReset()
	{
		UpdateStoreButtons();
		//
		Group.SetActive(true);
        PlayButton.interactable = true;
		//
		GameModeGroup.SetActive(SaveProfile.Instance.FTUEComplete);
		OnGameModeChange((int)SaveProfile.Instance.LevelMode);
	}


	private void GameStarted()
    {
        Group.SetActive(false);
    }

	public void OnGameModeChange()
	{
		SaveProfile.Instance.LevelMode = (Types.ELevelMode)GameModeSlider.value;
		SaveProfile.Instance.MimeMode &= (SaveProfile.Instance.LevelMode == Types.ELevelMode.Fast);
		//
		MimeMode.SetActive(SaveProfile.Instance.LevelMode == Types.ELevelMode.Fast);
		MimeModeToggle.isOn = SaveProfile.Instance.MimeMode;
	}

	public void OnGameModeChange(int value)
	{
		GameModeSlider.value = value;
		OnGameModeChange();
	}

	public void OnMimeToggleChange()
	{
		SaveProfile.Instance.MimeMode = MimeModeToggle.isOn;
	}

	public void QuitPressed()
	{
		SaveProfile.Instance.SaveData();
		Application.Quit();
	}

	private void StoreInit(bool init)
	{
		if (!init)
			return;

		UpdateStoreButtons();
	}

	private void IAPComplete(string IAPName, bool success)
	{
		GameManager.Instance.ProcessIAP(IAPName, success);
		UpdateStoreButtons();
	}

	#endregion


	#region Coroutine

	private IEnumerator BannerCoroutine()
	{
		Novulo.Adverts.AdsManager.Get.LoadBanner();
		//
		while (!SaveProfile.Instance.RemoveAds)
		{
			do { yield return new WaitForSeconds(0.25f); } while (!Novulo.Adverts.AdsManager.Get.IsBannerReady());
			//
			Novulo.Adverts.AdsManager.Get.ShowBanner();
			//
			do { yield return new WaitForSeconds(0.25f); } while (Novulo.Adverts.AdsManager.Get.IsBannerShowing() && !SaveProfile.Instance.RemoveAds);
		}
		//
		Novulo.Adverts.AdsManager.Get.HideBanner();
	}

	#endregion
}

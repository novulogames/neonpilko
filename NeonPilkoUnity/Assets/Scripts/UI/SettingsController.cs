﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;


public class SettingsController : MonoBehaviour, Novulo.Platform.Common.IBackButtonListener
{
	#region Events

	public delegate void MusicSettingCallback();
	public static MusicSettingCallback OnMusicSetting;

	public delegate void SFXSettingCallback();
	public static SFXSettingCallback OnSFXSetting;

	#endregion


	#region Members

	public Button MusicToggle;
	public Button SFXToggle;

	#endregion


	#region MonoBehaviour

	private void Start ()
	{
        UpdateButtons();
    }

	private void OnEnable()
	{
		Novulo.Platform.Common.BackButtonHandler.Get.RegisterListener(this);
	}

	private void OnDisable()
	{
		Novulo.Platform.Common.BackButtonHandler.Get?.UnregisterListener(this);
	}

	#endregion


	#region IBackButtonListener

	public void HandleBackButton()
	{
		PopupManager.Get.EnablePopup(Types.EPopupTypes.Settings, false);
	}

	#endregion


	#region Internal

	private void UpdateButtons()
    {
        MusicToggle.GetComponent<Image>().color = SaveProfile.Instance.Music ? Color.green : Color.red;
        SFXToggle.GetComponent<Image>().color = SaveProfile.Instance.SFX ? Color.green : Color.red;
    }

    #endregion


    #region Callbacks

    public void MusicPressed()
	{
		AchievementsManager.Instance.IncrementAchievement(AchievementsManager.AchvName.ElectroHate, 1);
		
		//
		
		SaveProfile.Instance.Music = !SaveProfile.Instance.Music;
        UpdateButtons();

		if (OnMusicSetting != null)
			OnMusicSetting();

        //
        AnalyticsEvent.Custom("music_toggle", new Dictionary<string, object>()
        {
            { "enable", SaveProfile.Instance.Music }
        });  
	}

	public void SFXPressed()
	{
		SaveProfile.Instance.SFX = !SaveProfile.Instance.SFX;
        UpdateButtons();
		
		if (OnSFXSetting != null)
			OnSFXSetting();

        //
        AnalyticsEvent.Custom("sfx_toggle", new Dictionary<string, object>()
        {
            { "enable", SaveProfile.Instance.SFX }
        });  
	}


	public void LanguagePressed()
	{
		SceneLoader.LoadLanguageScene();

        //
        AnalyticsEvent.Custom("langauge_pressed", new Dictionary<string, object>()
        {
            { "lang", SaveProfile.Instance.GameLanguage }
        });  
	}


	public void CreditsPressed()
	{
		AchievementsManager.Instance.IncrementAchievement(AchievementsManager.AchvName.SecretAdmirer, 1);
		gameObject.SetActive(false);

        //
        AnalyticsEvent.Custom("credits_pressed", new Dictionary<string, object>()); 
	}

	public void RestorePurchases()
	{
		Novulo.Store.IAPManager.Get.RestorePurchases();
	}

	#endregion
}

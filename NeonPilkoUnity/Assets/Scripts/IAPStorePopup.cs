﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IAPStorePopup : MonoBehaviour, Novulo.Platform.Common.IBackButtonListener
{
	#region Members

	public Button RemoveAdsButton;
	public Button FullLifeButton;
	public Button CloseButton;

	#endregion


	#region MonoBehaviour

	private void OnEnable()
	{
		if (!Novulo.Store.IAPManager.Get.IsInitialised)
		{
			RemoveAdsButton.interactable = false;
			FullLifeButton.interactable = false;
		}
		else
		{
			RemoveAdsButton.interactable = true;
			FullLifeButton.interactable = true;
		}
		CloseButton.interactable = true;
		//
		UpdateButtons();
		//
		Novulo.Store.IAPManager.OnIAPPurchaseComplete += IAPComplete;
		Novulo.Platform.Common.BackButtonHandler.Get.RegisterListener(this);
	}

	private void OnDisable()
	{
		Novulo.Store.IAPManager.OnIAPPurchaseComplete -= IAPComplete;
		Novulo.Platform.Common.BackButtonHandler.Get?.UnregisterListener(this);
	}

	#endregion


	#region IBackButtonListener

	public void HandleBackButton()
	{
		if (!CloseButton.interactable)
			return;

		PopupManager.Get.CloseAllPopups();
	}

	#endregion


	#region Internal

	private void UpdateButtons()
	{
		RemoveAdsButton.gameObject.SetActive(SaveProfile.Instance.RemoveAds == false);
		FullLifeButton.gameObject.SetActive(SaveProfile.Instance.FullHealth == false);
	}

	#endregion


	#region Callbacks

	private void IAPComplete(string IAPName, bool success)
	{
		GameManager.Instance.ProcessIAP(IAPName, success);
		//
		RemoveAdsButton.interactable = true;
		FullLifeButton.interactable = true;
		CloseButton.interactable = true;
		//
		UpdateButtons();
	}

	public void RemoveAdsPressed()
	{
		if (!RemoveAdsButton.interactable)
			return;

		RemoveAdsButton.interactable = false;
		FullLifeButton.interactable = false;
		CloseButton.interactable = false;

		Novulo.Store.IAPManager.Get.Purchase("neonball.removeads");
	}

	public void FullHealthPressed()
	{
		if (!FullLifeButton.interactable)
			return;

		RemoveAdsButton.interactable = false;
		FullLifeButton.interactable = false;
		CloseButton.interactable = false;

		Novulo.Store.IAPManager.Get.Purchase("neonball.fullhealth");
	}

	#endregion
}

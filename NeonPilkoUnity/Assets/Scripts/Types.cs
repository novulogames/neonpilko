﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Types
{	public enum GridPosition
	{
		BottomLeft = 0,
		BottomMiddle,
		BottomRight,
		MiddleLeft,
		Middle,
		MiddleRight,
		UpperLeft,
		UpperMiddle,
		UpperRight,

		Hidden,

		Noof
	}

	public enum ESwipeDirection
	{
		Left = 0,
		Right,
		Up,
		Down,

		Noof
	}

	public enum EPopupTypes
	{
		Life,
		AdFailed,
		AdSkip,
		Settings,
		Credits,
		Support,
		Store,

		Noof
	}

	public enum ELevelMode
	{
		Slow = 0,
		Fast,

		Noof
	}
}
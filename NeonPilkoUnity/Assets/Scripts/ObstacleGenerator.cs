﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour
{
    #region Events

    public delegate void SpeedChangeCallback(float speed);
    public static SpeedChangeCallback OnSpeedChange;

    #endregion


    #region Members

    public Transform[] ObstacleList;
	public Transform[] FTUEObstaclesList;

    public Transform StartPath;
    public Transform EndPath;
    public Transform ObstaclesParent;

	public LevelDefinitionAsset[] Definitions;

    private float m_currentFreq;
    private float m_currentSpeed;
    private float m_timer = 0f;

	private bool m_active = false;

	private LevelDefinitionAsset m_currentDefinition;
    private static ObstacleGenerator ms_instance = null;

    #endregion


    #region Properties

    public static ObstacleGenerator Instance
    {
        get { return ms_instance; }
    }

    public float CurrentFrequency
    {
        get { return m_currentFreq; }
    }

    public float CurrentSpeed
    {
        get { return m_currentSpeed; }
    }

    #endregion


    #region MonoBehaviour

    private void Awake()
    {
        GameManager.OnGameStarted += GameStarted;
        GameManager.OnGameEnd += GameEnded;
        BallController.OnScoreUpdate += SpeedUpGeneration;
        //
        ms_instance = this;
    }

    private void OnDestroy()
    {
        GameManager.OnGameStarted -= GameStarted;
        GameManager.OnGameEnd -= GameEnded;
        BallController.OnScoreUpdate -= SpeedUpGeneration;
        //
        ms_instance = null;
    }

    void Start ()
    {
        m_active = false;
        m_timer = 0f;
	}

	#endregion


	#region Methods

	public void Spawn(int index)
	{
		Transform t = Instantiate(ObstacleList[index]);
		t.position = StartPath.position;
		t.SetParent(ObstaclesParent);

		ObstacleBaseController obs = t.GetComponent<ObstacleBaseController>();
		obs.Setup(StartPath.position, EndPath.position, m_currentSpeed);
	}

	public ObstacleBaseController SpawnFTUE(int index)
	{
		Transform t = Instantiate(FTUEObstaclesList[index]);
		t.position = StartPath.position;
		t.SetParent(ObstaclesParent);

		ObstacleBaseController obs = t.GetComponent<ObstacleBaseController>();
		obs.Setup(StartPath.position, EndPath.position, m_currentSpeed * 0.5f);
		return obs;
	}

	#endregion


	#region Callback

	private void GameStarted()
    {
		m_currentDefinition = Definitions[(int)SaveProfile.Instance.LevelMode];
		//
		m_active = true;
        m_timer = m_currentDefinition.FrequencyMax;
        m_currentFreq = m_currentDefinition.FrequencyMax;
        m_currentSpeed = m_currentDefinition.SpeedMin;
		//
		ObstacleBaseController.GlobalSpeed = 1f;
		StartCoroutine(UpdateCoroutine());
	}

    private void GameEnded()
    {
		StopAllCoroutines();
        m_active = false;
		ObstacleBaseController.GlobalSpeed = 0f;

		StartCoroutine(GameOverCoroutine());
    }

    private void SpeedUpGeneration(int score)
    {
		// No speed up during FTUE
		if (SaveProfile.Instance.FTUEComplete)
			return;

        m_currentFreq -= m_currentDefinition.FrequencyIncrement;
        m_currentFreq = Mathf.Max(m_currentFreq, m_currentDefinition.FrequencyMin);

        m_currentSpeed += (m_currentDefinition.SpeedMin * m_currentDefinition.SpeedIncrement);
        m_currentSpeed = Mathf.Min(m_currentSpeed, m_currentDefinition.SpeedMax);

        if (OnSpeedChange != null)
            OnSpeedChange(m_currentDefinition.FrequencyMax / m_currentFreq);
    }

	#endregion


	#region Coroutine

	private IEnumerator UpdateCoroutine()
	{
		do
		{ yield return new WaitForEndOfFrame(); }
		while (!SaveProfile.Instance.FTUEComplete);

		while (m_active)
		{
			m_timer += Time.deltaTime;
			if (m_timer >= m_currentFreq)
			{
				m_timer -= m_currentFreq;

				int idx = Random.Range(0, 100) % ObstacleList.Length;
				Spawn(idx);
			}
			//
			yield return new WaitForEndOfFrame();
		}
	}

	private IEnumerator GameOverCoroutine()
	{
		int childCount = ObstaclesParent.childCount;
		//
		for (int i = childCount - 1; i >= 0; --i)
		{
			ObstacleBaseController obs = ObstaclesParent.GetChild(i).gameObject.GetComponent<ObstacleBaseController>();
			obs.PauseObstacle();
		}
		//
		yield return new WaitForSeconds(1f);
		//
		for (int i = childCount - 1; i >= 0; --i)
		{
			ObstacleBaseController obs = ObstaclesParent.GetChild(i).gameObject.GetComponent<ObstacleBaseController>();
			obs.FadeOut();
			//
			yield return new WaitForSeconds(0.2f);
		}
	}

	#endregion
}

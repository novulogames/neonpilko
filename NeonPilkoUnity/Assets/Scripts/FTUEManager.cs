﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FTUEManager : MonoBehaviour
{
	#region Members

	private Animator m_anim = null;

	#endregion


	#region MonoBehaviour

	private void Awake()
	{
		if (SaveProfile.Instance.FTUEComplete)
		{
			CleanFTUE();
			return;
		}

		GameManager.OnGameStarted += GameStarted;
		GameManager.OnGameEnd += GameEnd;
	}

	private void OnDestroy()
	{
		GameManager.OnGameStarted -= GameStarted;
		GameManager.OnGameEnd -= GameEnd;
	}

	private void Start()
	{
		m_anim = GetComponent<Animator>();
	}

	#endregion


	#region Methods

	public void CleanFTUE()
	{
		Destroy(gameObject);
	}

	#endregion


	#region Callback

	private void GameStarted()
	{
		StartCoroutine(DoFTUECoroutine());
	}

	private void GameEnd()
	{
		StopAllCoroutines();
		// Reset animation
		m_anim.SetTrigger("reset");
	}

	#endregion


	#region Coroutine

	private IEnumerator DoFTUECoroutine()
	{
		//
		// PART 1
		//
		yield return new WaitForSeconds(2f);

		// Generate single vertical bar
		ObstacleBaseController obs = ObstacleGenerator.Instance.SpawnFTUE(0);

		// Play horizontal animation
		m_anim.SetTrigger("h");

		// Wait for obstacle to approach
		yield return new WaitForSeconds(3.25f);

		// Make sure player is on the side
		if (BallController.Instance.CurrentPosition == Types.GridPosition.BottomMiddle ||
				BallController.Instance.CurrentPosition == Types.GridPosition.Middle ||
				BallController.Instance.CurrentPosition == Types.GridPosition.UpperMiddle)
		{
			obs.SlowMotion(true, 0.8f);
			//
			do
			{ yield return new WaitForEndOfFrame(); }
			while (BallController.Instance.CurrentPosition == Types.GridPosition.BottomMiddle ||
					BallController.Instance.CurrentPosition == Types.GridPosition.Middle ||
					BallController.Instance.CurrentPosition == Types.GridPosition.UpperMiddle);
			//
			obs.SlowMotion(false, 1.25f);
		}
		//
		yield return new WaitForSeconds(2f);

		// Reset animation
		m_anim.SetTrigger("reset");

		//
		// PART 2
		//
		yield return new WaitForSeconds(2f);

		obs = ObstacleGenerator.Instance.SpawnFTUE(1);

		// Play vertical animation
		m_anim.SetTrigger("v");

		// Wait for obstacle to approach
		yield return new WaitForSeconds(3.25f);

		// Make sure player is on the side
		if (BallController.Instance.CurrentPosition == Types.GridPosition.MiddleLeft ||
				BallController.Instance.CurrentPosition == Types.GridPosition.Middle ||
				BallController.Instance.CurrentPosition == Types.GridPosition.MiddleRight)
		{
			obs.SlowMotion(true, 0.8f);
			//
			do { yield return new WaitForEndOfFrame(); }
			while (BallController.Instance.CurrentPosition == Types.GridPosition.MiddleLeft ||
					BallController.Instance.CurrentPosition == Types.GridPosition.Middle ||
					BallController.Instance.CurrentPosition == Types.GridPosition.MiddleRight);
			//
			obs.SlowMotion(false, 1.25f);
		}
		//
		yield return new WaitForSeconds(2f);

		// Reset animation
		m_anim.SetTrigger("reset");

		//
		// PART 3
		//
		ObstacleGenerator.Instance.SpawnFTUE(2);

		//
		// COMPLETE
		//
		yield return new WaitForSeconds(5f);
		SaveProfile.Instance.FTUEComplete = true;
		CleanFTUE();
	}

	#endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager : MonoBehaviour 
{
	#region Members

	public GameObject[] Lights;
	public Transform StartPoint;
	public Transform EndPoint;

	public float Speed;

    private float m_currentSpeed = 0f;
    private float m_originIntensity;
    private float m_originRange;

    #endregion


    #region MonoBehaviour

    private void Awake()
    {
        ObstacleGenerator.OnSpeedChange += UpdateSpeed;
        GameManager.OnGameEnd += GameEnded;
    }

    void Start ()
    {
        int count = Lights.Length;
        for (int idx = 0; idx < count; ++idx)
        {
            m_originIntensity = Lights[idx].GetComponent<Light>().intensity;
            m_originRange = Lights[idx].GetComponent<Light>().range;
        }

        Reset();
	}

	void OnDestroy ()
	{
        ObstacleGenerator.OnSpeedChange -= UpdateSpeed;
        GameManager.OnGameEnd -= GameEnded;
        StopAllCoroutines();
	}

    #endregion


    #region Internal

    private void Reset()
    {
        StopAllCoroutines();

        //

        m_currentSpeed = Speed;

        int count = Lights.Length;
        for (int idx = 0; idx < count; ++idx)
        {
            float delay = idx * (1f / count);// 1f - ((float)idx / (float)count);
            IEnumerator coroutine = UpdateLight(idx, delay);
            StartCoroutine(coroutine);

            coroutine = BounceLight(idx);
            StartCoroutine(coroutine);
        }
    }

    #endregion


    #region Callbacks

    private void UpdateSpeed(float multipler)
    {
        multipler = Mathf.Max(1f, multipler * 0.65f);
        m_currentSpeed = Speed * multipler;
    }

    private void GameEnded()
    {
        Reset();
    }

    #endregion


    #region Coroutine

    // Update is called once per frame
    private IEnumerator UpdateLight (int index, float delay)
    {
		float timer = delay;

		// Activate lights
		Lights[index].SetActive(true);
		Transform t = Lights[index].transform;

		// Move lights across tunnel
		while (true)
		{
			timer += (Time.deltaTime * m_currentSpeed);
			t.position = Vector3.Lerp(StartPoint.position, EndPoint.position, timer);

            if (timer >= 1f)
                timer -= 1f;

			yield return new WaitForEndOfFrame();
		}
	}

    private IEnumerator BounceLight(int index)
    {
        float timer = 0f;
        float bounceTime = 0.35f;
        float direction = 1f;

        Light l = Lights[index].GetComponent<Light>();
        float targetInten = m_originIntensity * 0.8f;
        float targetRange = m_originRange * 0.8f;

        while (true)
        {
            timer += (Time.deltaTime * direction);
            timer = Mathf.Clamp(timer, 0, bounceTime);
            l.intensity = Mathf.Lerp(m_originIntensity, targetInten, timer / bounceTime);
            l.range = Mathf.Lerp(m_originRange, targetRange, timer / bounceTime);

            if (timer >= bounceTime || timer <= 0f)
                direction *= -1f;

            yield return new WaitForEndOfFrame();
        }
    }

	#endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;


public class SaveProfile : MonoBehaviour
{
	#region Types

    [System.Serializable]
    private class PlayerData
    {
		public Language.Lang GameLang = Language.Lang.Noof;
        public int BestScore = 0;
		public int VideoCounter = 0;

		public int TotalLifes = 1;
		public float ExtraLifeEndTime;
		public System.DateTime ExtraLifeStartTime;

		public uint TotalCrash = 0;
		public uint TotalSwipes = 0;
		public uint TotalSessions = 0;

		public bool MusicOn = true;
		public bool SFXOn = true;

		public bool ftueComplete = false;

		public Types.ELevelMode levelMode = Types.ELevelMode.Slow;
		public bool mimeMode = false;

		public bool removeAds = false;
		public bool fullLifes = false;

		public bool gdprAccepted = false;
    }

    #endregion


	#region Members

	public const int MaxVideoCount = 2;
	public const int MaxLifes = 3;
	
	private const string FILE_NAME = "save.bin";

	private PlayerData m_data;
	private bool m_dirty = false;

	private static SaveProfile ms_instance = null;

	#endregion


	#region Properties

	public static SaveProfile Instance
	{
		get { return ms_instance; }
	}

	public Language.Lang GameLanguage
	{
		get { return m_data.GameLang; }
		set
		{
			m_data.GameLang = value;
			m_dirty = true;
		}
	}

	public int BestScore
	{
		get { return m_data.BestScore; }
		set 
		{ 
			m_data.BestScore = value; 
			m_dirty = true;
		}
	}

	public int Lifes
	{
		get
		{
			if (FullHealth)
				return MaxLifes;

			return m_data.TotalLifes;
		}
		set 
		{
			if (FullHealth)
				m_data.TotalLifes = MaxLifes;
			else
				m_data.TotalLifes = Mathf.Min(MaxLifes, value);

			m_dirty = true;
		}
	}

	public float ExtraLifeEndTime
	{
		get { return m_data.ExtraLifeEndTime; }
		set 
		{
			m_data.ExtraLifeStartTime = System.DateTime.Now;
			m_data.ExtraLifeEndTime = Mathf.Max(0f, value);

			if (m_data.ExtraLifeEndTime == 0)
				Lifes = 1;
		}
	}

	public int VideoCounter
	{
		get { return m_data.VideoCounter; }
		set 
		{ 
			m_data.VideoCounter = value; 
			m_dirty = true;
		}
	}

	public uint Crashes
	{
		get { return m_data.TotalCrash; }
		set
		{
			m_data.TotalCrash = value;
			m_dirty = true;
		}
	}

	public uint Swipes
	{
		get { return m_data.TotalSwipes; }
		set
		{
			m_data.TotalSwipes = value;
			m_dirty = true;
		}
	}

	public uint SessionsPlayed
	{
		get { return m_data.TotalSessions; }
		set
		{
			m_data.TotalSessions = value;
			m_dirty = true;
		}
	}

	public bool Music
	{
		get { return m_data.MusicOn; }
		set
		{ 
			m_data.MusicOn = value;
			m_dirty = true;
		}
	}

	public bool SFX
	{
		get { return m_data.SFXOn; }
		set
		{
			m_data.SFXOn = value;
			m_dirty = true;
		}
	}

	public bool FTUEComplete
	{
		get { return m_data.ftueComplete; }
		set
		{
			m_data.ftueComplete = value;
			m_dirty = true;
		}
	}

	public Types.ELevelMode LevelMode
	{
		get { return m_data.levelMode; }
		set
		{
			m_data.levelMode = value;
			m_dirty = true;
		}
	}

	public bool MimeMode
	{
		get { return m_data.mimeMode; }
		set
		{
			m_data.mimeMode = value;
			m_dirty = true;
		}
	}

	public bool RemoveAds
	{
		get { return m_data.removeAds; }
		set
		{
			m_data.removeAds = value;
			m_dirty = true;
		}
	}

	public bool FullHealth
	{
		get { return m_data.fullLifes; }
		set
		{
			m_data.fullLifes = value;
			m_dirty = true;
		}
	}

	public bool GDPRAccepted
	{
		get { return m_data.gdprAccepted; }
		set
		{
			m_data.gdprAccepted = value;
			m_dirty = true;
		}
	}

	#endregion


	#region MonoBehaviour

	void Awake ()
	{
		GameManager.OnGameInit += GameInit;
		//
		ms_instance = this;
		m_data = new PlayerData();
	}


	void OnApplicationPause (bool pause)
	{
		if (pause)
			SaveData();
	}
	void OnApplicationQuit ()
	{
		SaveData();
	}


	void OnDestroy() 
	{
		GameManager.OnGameInit -= GameInit;
		//
		ms_instance = null;
		StopAllCoroutines();
	}

	#endregion


	#region Methods

	public void LoadData()
	{
		string fullPath = System.IO.Path.Combine(Application.persistentDataPath, FILE_NAME);
		if (!System.IO.File.Exists(fullPath))
			m_data = new PlayerData();
		else
			m_data = Novulo.IO.DiskIO.ReadData<PlayerData>(fullPath);

		//
		System.TimeSpan ts = System.DateTime.Now - m_data.ExtraLifeStartTime;
		ExtraLifeEndTime -= (float)ts.TotalSeconds;
	}


	public void SaveData()
	{ 
		string fullPath = System.IO.Path.Combine(Application.persistentDataPath, FILE_NAME);
		Novulo.IO.DiskIO.WriteData<PlayerData>(fullPath, m_data);
	}

	#endregion


	#region Callbacks

	private void GameInit()
	{
		IEnumerator coroutine = UpdateExtraLifeTime();
		StartCoroutine(coroutine);

		coroutine = SaveDataCoroutine();
		StartCoroutine(coroutine);
	}

	#endregion


	#region Coroutine

	private IEnumerator UpdateExtraLifeTime()
	{
		float saveTimer = 0f;
		float SaveInterval = 60f;
		float dt = 0f;

		while(true)
		{
			dt = Time.deltaTime;
			ExtraLifeEndTime -= dt;
			
			saveTimer += dt;
			if (saveTimer >= SaveInterval)
			{
				saveTimer = 0f;
				m_dirty = true;
			}

			yield return new WaitForEndOfFrame();
		}
	}


	private IEnumerator SaveDataCoroutine()
	{
		while(true)
		{
			yield return new WaitForSeconds(5f);
			//
			if (m_dirty)
			{
				SaveData();
				m_dirty = false;
				yield return new WaitForEndOfFrame();
			}
		}
	}

	#endregion
}

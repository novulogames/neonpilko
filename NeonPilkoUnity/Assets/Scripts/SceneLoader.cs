﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneLoader : MonoBehaviour 
{
	#region Methods

	public static void LoadMainScene()
	{
		SceneManager.LoadScene("tube");
	}

	public static void LoadLanguageScene()
	{
		SceneManager.LoadScene("language");
	}

	public static void LoadGDPR()
	{
		SceneManager.LoadScene("gdpr");
	}

	#endregion
}

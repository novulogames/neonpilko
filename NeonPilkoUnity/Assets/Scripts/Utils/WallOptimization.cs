﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class WallOptimization : MonoBehaviour
{
	#region Members

	public GameObject Top;
	public GameObject Left;
	public GameObject Right;

	#endregion


	#region MonoBehaviour

	private void Start()
	{
#if UNITY_EDITOR
		return;
#elif UNITY_IOS
		return;
#elif UNITY_ANDROID
		if (SystemInfo.graphicsDeviceType == GraphicsDeviceType.Vulkan)
			return;

		Top.SetActive(SystemInfo.graphicsDeviceType == GraphicsDeviceType.OpenGLES3);
		Left.SetActive(false);
		Right.SetActive(false);
#endif
	}

	#endregion
}

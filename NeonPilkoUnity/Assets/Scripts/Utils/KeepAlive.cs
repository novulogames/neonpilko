﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepAlive : MonoBehaviour 
{
	#region MonoBehaviour

	void Awake () 
	{
		DontDestroyOnLoad(gameObject);
	}

	#endregion
}

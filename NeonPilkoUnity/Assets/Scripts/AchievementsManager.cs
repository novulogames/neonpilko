﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Analytics;


public class AchievementsManager : MonoBehaviour 
{
	#region Types

	[System.Serializable]
	public enum AchvName
	{
		FirstImpact = 0,
		ThatHurt,
		WhatPain,
		NoSweat,
		FigerOnFire,
		CallMeSir,
		ElectroHate,
		SecretAdmirer,
		TitleSmasher,
		GameGood,
		SwipeForest,
		Machine,
		Saluton,

		Noof
	};


	[System.Serializable]
	public class AchvData
	{
		public string ID;
		public int CurrentStep;
		public int TotalSteps;
		public bool Unlocked;

		public AchvData(string id, int currentStep, int totalSteps)
		{
			ID = id;
			CurrentStep = currentStep;
			TotalSteps = totalSteps;
			Unlocked = false;
		}

		public bool ShouldUnlock()
		{
			return (CurrentStep >= TotalSteps);
		}

		public float Progress
		{
			get { return CurrentStep / TotalSteps; }
		}
	}

	#endregion


	#region Members

	private const string FOLDER_NAME = "novulo";
	private const string FILE_NAME = "achv.bin";

	private Dictionary<AchvName, AchvData> m_achievements = new Dictionary<AchvName, AchvData>()
	{
		{ AchvName.FirstImpact	, new AchvData( "CgkI_rKM-_wBEAIQAg", 0, 1		) },
		{ AchvName.ThatHurt		, new AchvData( "CgkI_rKM-_wBEAIQAw", 0, 100	) },
		{ AchvName.WhatPain		, new AchvData( "CgkI_rKM-_wBEAIQBA", 0, 1000	) },
		{ AchvName.NoSweat		, new AchvData( "CgkI_rKM-_wBEAIQDQ", 0, 100	) },
		{ AchvName.FigerOnFire	, new AchvData( "CgkI_rKM-_wBEAIQDg", 0, 250	) },
		{ AchvName.CallMeSir	, new AchvData( "CgkI_rKM-_wBEAIQDw", 0, 500	) },
		{ AchvName.ElectroHate	, new AchvData( "CgkI_rKM-_wBEAIQCA", 0, 1		) },
		{ AchvName.SecretAdmirer, new AchvData( "CgkI_rKM-_wBEAIQCQ", 0, 1		) },
		{ AchvName.TitleSmasher	, new AchvData( "CgkI_rKM-_wBEAIQCg", 0, 1		) },
		{ AchvName.GameGood		, new AchvData( "CgkI_rKM-_wBEAIQBQ", 0, 100	) },
		{ AchvName.SwipeForest	, new AchvData( "CgkI_rKM-_wBEAIQBg", 0, 500	) },
		{ AchvName.Machine		, new AchvData( "CgkI_rKM-_wBEAIQBw", 0, 1000	) },
		{ AchvName.Saluton		, new AchvData( "CgkI_rKM-_wBEAIQEA", 0, 1		) },
	};

	private bool m_dirty = false;
	private static AchievementsManager ms_instance = null;

	#endregion


	#region Properties

	public static AchievementsManager Instance
	{
		get { return ms_instance; }
	}

	#endregion


	#region MonoBehaviour

	void Awake () 
	{
		GameManager.OnGameInit += GameInit;
		//
		ms_instance = this;	
	}
	
	void OnDestroy () 
	{
		GameManager.OnGameInit -= GameInit;
		//
		ms_instance = null;
		StopAllCoroutines();
	}

	#endregion


	#region Methods

	public void LoadData()
	{
		string fullPath = string.Format("{0}/{1}", Application.persistentDataPath, FOLDER_NAME);
		if (!Directory.Exists(fullPath))
			return;

		fullPath = string.Format("{0}/{1}", fullPath, FILE_NAME);		
		try 
		{
			FileStream fileStream = File.Open(fullPath, FileMode.Open, FileAccess.Read);
			BinaryFormatter formatter = new BinaryFormatter();
			m_achievements = (Dictionary<AchvName, AchvData>)formatter.Deserialize(fileStream);
			fileStream.Close();
		} 
		catch(Exception e) 
		{
			Debug.LogWarning("SaveLoad.LoadFile(): Failed to deserialize a file " + fullPath + " (Reason: " + e.ToString() + ")");
		}

		//
		UnlockOnLoad();
	}


	public void SaveData()
	{ 
		string fullPath = string.Format("{0}/{1}",Application.persistentDataPath, FOLDER_NAME);
		if (!Directory.Exists(fullPath))
			Directory.CreateDirectory(fullPath);

		fullPath = string.Format("{0}/{1}", fullPath, FILE_NAME);
		try 
		{
			FileStream fileStream = File.Open(fullPath, FileMode.Create);
			BinaryFormatter formatter = new BinaryFormatter();
			formatter.Serialize(fileStream, (object)m_achievements);
			fileStream.Close();
		} 
		catch(Exception e) 
		{
			Debug.LogWarning("Save.SaveFile(): Failed to serialize object to a file " + fullPath + " (Reason: " + e.ToString() + ")");
        }
	}


	public void IncrementAchievement(AchvName name, int value)
	{
		AchvData data = m_achievements[name];
		if (data.Unlocked)
			return;
			
		data.CurrentStep += value;

		//

		if (data.ShouldUnlock())
		{
			Novulo.Social.SocialManager.Get.UnlockAchievement(data.ID);
			data.Unlocked = true;

			//
			AnalyticsEvent.Custom("achievement_unlock", new Dictionary<string, object>()
			{
				{ "id", name.ToString() }
			});  
		}
		else
		{
			Novulo.Social.SocialManager.Get.UpdateAchievement(data.ID, value);
		}
			
		// Save progress
		m_dirty = true;
	}


	public void UpdateAchievement(AchvName name, int value)
	{
		AchvData data = m_achievements[name];
		if (data.Unlocked)
			return;

		data.CurrentStep = value;

		//

		if (data.ShouldUnlock())
		{
			Novulo.Social.SocialManager.Get.UnlockAchievement(data.ID);
			data.Unlocked = true;
			
			//
			AnalyticsEvent.Custom("achievement_unlock", new Dictionary<string, object>()
			{
				{ "id", name.ToString() }
			});  
			
			// Save progress
			m_dirty = true;
		}
	}

	#endregion


	#region Internal

	private void UnlockOnLoad()
	{
		foreach(KeyValuePair<AchvName, AchvData> kvp in m_achievements)
		{
			if (kvp.Value.Unlocked)
			{
				Novulo.Social.SocialManager.Get.UnlockAchievement(kvp.Value.ID);
			}
		}
	}

	#endregion


	#region Callbacks

	private	void GameInit()
	{
		IEnumerator coroutine = SaveDataCoroutine();
		StartCoroutine(coroutine);
	}

	#endregion


	#region Coroutine

	private IEnumerator SaveDataCoroutine()
	{
		while(true)
		{
			yield return new WaitForSeconds(5f);
			//
			if (m_dirty)
			{
				SaveData();
				m_dirty = false;
				yield return new WaitForEndOfFrame();
			}
		}
	}

	#endregion
}

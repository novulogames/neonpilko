﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleRotateController : ObstacleBaseController
{
    #region Members

    public Transform Column;
	public GameObject CrossColumn;
	[Space]
    public float RotationSpeedMax;
    public float RotationSpeedMin;

    private float m_rotationSpeed;
    private int m_direction = 0;

	#endregion


	#region MonoBehaviour

	new void Start()
	{
		base.Start();
		StartCoroutine(UpdateCoroutine());
	}

    #endregion


    #region ObstacleBaseController

    public override void Setup(Vector3 start, Vector3 end, float speed)
    {
        base.Setup(start, end, speed);

        // Random speed range
        m_rotationSpeed = Random.Range(RotationSpeedMin, RotationSpeedMax);

		//
		CrossColumn.SetActive(Random.Range(0, 100) < 50);

			// Random rotation direction
		m_direction = (Random.Range(0, 100) <= 50) ? 1 : -1;
    }

	#endregion


	#region Coroutine

	private IEnumerator UpdateCoroutine()
	{
		float t = 0f;
		float dt = 0f;

		while (true)
		{
			t = Time.deltaTime * GlobalSpeed;
			dt = t * m_rotationSpeed * m_direction;
			Column.transform.Rotate(dt, 0f, 0f, Space.Self);
			CrossColumn.transform.Rotate(dt, 0f, 0f, Space.Self);
			//
			yield return new WaitForEndOfFrame();
		}
	}

	#endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovingColumnSingleController : ObstacleBaseController
{
    #region Members

    public GameObject LeftColumn;
    public GameObject MiddleColumn;
    public GameObject RightColumn;
    public GameObject MovingColumn;

    public float MoveSpeedMax;
    public float MoveSpeedMin;

    private Transform m_originTransform;
    private Transform m_targetTransform;

    private float m_timer = 0f;
    private int m_direction = 1;
    private float m_speed = 0f;

	#endregion


	#region MonoBehaviour
		new void Start()
	{
		base.Start();

		IEnumerator coroutine = UpdateCoroutine();
		StartCoroutine(coroutine);
	}

    #endregion


    #region ObstacleBaseController

    public override void Setup(Vector3 start, Vector3 end, float speed)
    {
        base.Setup(start, end, speed);

        //
        m_direction = (Random.Range(0, 100) < 50) ? 1 : -1;

        bool horizontal = (Random.Range(0, 100) <= 50);
        if (horizontal)
        {
            transform.Rotate(90f * m_direction, 0f, 0f, Space.Self);
        }

        //

        Dictionary<int, GameObject> mapColumn = new Dictionary<int, GameObject>();

        int rand = Random.Range(0, 9999);
        mapColumn.Add(rand, LeftColumn);

        do
        {
            rand = Random.Range(0, 9999);
        }
        while (mapColumn.ContainsKey(rand));
        mapColumn.Add(rand, MiddleColumn);

        do
        {
            rand = Random.Range(0, 9999);
        }
        while (mapColumn.ContainsKey(rand));
        mapColumn.Add(rand, RightColumn);

		foreach (KeyValuePair<int, GameObject> kvp in mapColumn)
		{
			kvp.Value.SetActive(false);
		}

		//

		int[] keyArray = new int[3];
        var ad = mapColumn.Keys;
        ad.CopyTo(keyArray, 0);
        System.Array.Sort(keyArray);

        //

        MovingColumn.SetActive(true);

        m_originTransform = mapColumn[keyArray[0]].transform;
        m_targetTransform = mapColumn[keyArray[1]].transform;
/*
        m_originTransform = LeftColumn.transform;
        m_targetTransform = RightColumn.transform;
*/
        //

        m_speed = Random.Range(MoveSpeedMin, MoveSpeedMax);
    }

	#endregion


	#region Coroutine

	private IEnumerator UpdateCoroutine()
	{
		float t = 0f;
		while (true)
		{
			t = Time.deltaTime * GlobalSpeed;
			m_timer += (t * m_direction * m_speed);
			m_timer = Mathf.Clamp(m_timer, 0f, 1f);

			MovingColumn.transform.position = Vector3.Lerp(m_originTransform.position, m_targetTransform.position, m_timer);

			if (m_timer <= 0f || m_timer >= 1f)
			{
				m_direction *= -1;
			}

			//
			yield return new WaitForEndOfFrame();
		}
	}

	#endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleRotateColumnController : ObstacleBaseController
{
    #region Types

    private enum AnimState
    {
        Wait = 0,
        Rotate,

        Noof
    }

    #endregion


    #region Members

    public GameObject LeftColumn;
    public GameObject MiddleColumn;
    public GameObject RightColumn;

    public float RotationSpeedMax;
    public float RotationSpeedMin;

    private int m_direction = 1;
    private float m_rotationSpeed = 0f;
    private float m_currentAngle = 0f;
    private float m_targetAngle = 0f;
	private float m_rotationDistance = 60f;

    private Vector3 m_middlePos;

    private AnimState m_state = AnimState.Wait;

    #endregion


    #region MonoBehaviour

    new void Update()
    {
        base.Update();

        switch (m_state)
        {
            case AnimState.Wait:
				{
					float distance = Vector3.Distance(transform.position, m_middlePos);
					if (distance <= m_rotationDistance)
					{
						m_state = AnimState.Rotate;
					}
				}
				break;

            case AnimState.Rotate:
				{
					float t = Time.deltaTime * GlobalSpeed;
					float dt = t * m_rotationSpeed * m_direction;
					m_currentAngle += dt;
					if (Mathf.Abs(m_currentAngle) > m_targetAngle)
					{
						float diff = Mathf.Abs(m_currentAngle) - m_targetAngle;
						dt -= (diff * m_direction);
					}

					// Do rotation
					transform.Rotate(dt, 0f, 0f, Space.Self);

					// Clap angle
					m_currentAngle = Mathf.Clamp(m_currentAngle, -m_targetAngle, m_targetAngle); 

					// Exit when rotation is complete
					if (Mathf.Abs(m_currentAngle) == m_targetAngle)
					{
						m_state = AnimState.Noof;
					}
				}
				break;
        }
    }

    #endregion


    #region ObstacleBaseController

    public override void Setup(Vector3 start, Vector3 end, float speed)
    {
        base.Setup(start, end, speed);

        //
        m_middlePos = start + ((end - start) / 2.1f);

        //
        m_direction = (Random.Range(0, 100) < 50) ? 1 : -1;
        int range = Random.Range(0, 100) % 2;
        switch (range)
        {
            case 0: m_targetAngle = 180; m_rotationDistance = 80f; break;
			case 1: m_targetAngle = 90; m_rotationDistance = 60f; break;
        }

        bool horizontal = (Random.Range(0, 100) <= 50);
        if (horizontal)
        {
            transform.Rotate(90f * m_direction, 0f, 0f, Space.Self);
        }

		//
		if (Random.Range(0, 100) < 50)
		{
			// Single
			LeftColumn.SetActive(false);
			RightColumn.SetActive(false);
			MiddleColumn.SetActive(true);
		}
		else
		{
			LeftColumn.SetActive(false);
			RightColumn.SetActive(false);
			MiddleColumn.SetActive(false);

			//
			int rand = Random.Range(0, 100);
			if (rand <= 33)
			{
				LeftColumn.SetActive(true);
				RightColumn.SetActive(true);
			}
			else if (rand <= 66)
			{
				LeftColumn.SetActive(true);
				MiddleColumn.SetActive(true);
			}
			else
			{
				MiddleColumn.SetActive(true);
				RightColumn.SetActive(true);
			}
		}

        //
        m_rotationSpeed = Random.Range(RotationSpeedMin, RotationSpeedMax);
    }

    #endregion
}

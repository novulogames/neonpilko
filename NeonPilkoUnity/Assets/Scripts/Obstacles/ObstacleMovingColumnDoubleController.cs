﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovingColumnDoubleController : ObstacleBaseController
{
    #region Members

    public GameObject LeftColumn;
    public GameObject MiddleColumn;
    public GameObject RightColumn;
    public GameObject MovingColumn;

    public Transform LeftOrigin;
    public Transform LeftTarget;

    public Transform RightOrigin;
    public Transform RightTarget;

    public float MoveSpeedMax;
    public float MoveSpeedMin;

    private Transform m_originTransform;
    private Transform m_targetTransform;

    private float m_timer = 0f;
    private int m_direction = 1;
    private float m_speed = 0f;
    private float m_delay = 0.2f;

    #endregion


    #region MonoBehaviour

    new void Start()
    {
        base.Start();

        IEnumerator coroutine = UpdateCoroutine();
        StartCoroutine(coroutine);
    }

    #endregion


    #region ObstacleBaseController

    public override void Setup(Vector3 start, Vector3 end, float speed)
    {
        base.Setup(start, end, speed);

        //
        m_direction = (Random.Range(0, 100) < 50) ? 1 : -1;

        bool horizontal = (Random.Range(0, 100) <= 50);
        if (horizontal)
        {
            transform.Rotate(90f * m_direction, 0f, 0f, Space.Self);
        }

        //

        LeftColumn.SetActive(true);
        RightColumn.SetActive(true);
        MovingColumn.SetActive(true);
		MiddleColumn.SetActive(false);

        //
        
        int rand = Random.Range(0, 100);
        if (rand <= 50)
        {
            m_originTransform = RightOrigin;
            m_targetTransform = LeftTarget;
            LeftColumn.SetActive(false);
        }
        else
        {
            m_originTransform = LeftOrigin;
            m_targetTransform = RightTarget;
            RightColumn.SetActive(false);
        }

        //

        if (Random.Range(0, 100) > 50)
        {
            m_timer = 1f;
            m_direction = -1;
        }
        else
        {
            m_timer = 0f;
            m_direction = 1;
        }

        //
        m_speed = Random.Range(MoveSpeedMin, MoveSpeedMax);
        m_delay *= (m_speed * 2f);
    }

    #endregion


    #region Coroutine

    private IEnumerator UpdateCoroutine()
    {
		float t = 0f;
        while (true)
        {
			t = (Time.deltaTime * GlobalSpeed);
            m_timer += (t * m_direction * m_speed);
            m_timer = Mathf.Clamp(m_timer, 0f, 1f);

            MovingColumn.transform.position = Vector3.Lerp(m_originTransform.position, m_targetTransform.position, m_timer);

            if (m_timer <= 0f || m_timer >= 1f)
            {
                m_direction *= -1;
                m_timer = Mathf.Clamp(m_timer, 0f, 1f);
                //
                yield return new WaitForSeconds(m_delay);
            }

            //
            yield return new WaitForEndOfFrame();
        }
    }
    #endregion
}

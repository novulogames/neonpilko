﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleColumnController : ObstacleBaseController
{
    #region Members

    public GameObject LeftColumn;
    public GameObject MiddleColumn;
    public GameObject RightColumn;

	public float HorizontalChanges;

    #endregion


    #region MonoBehaviour

    #endregion


    #region ObstacleBaseController

    public override void Setup(Vector3 start, Vector3 end, float speed)
    {
        base.Setup(start, end, speed);

        //

        bool horizontal = (Random.Range(0, 100) <= HorizontalChanges);
        if (horizontal)
			Rotate();

        //

        Dictionary<int, GameObject> mapColumn = new Dictionary<int, GameObject>();

        int rand = Random.Range(0, 9999);
        mapColumn.Add(rand, LeftColumn);

        do
        {
            rand = Random.Range(0, 9999);
        }
        while (mapColumn.ContainsKey(rand));
        mapColumn.Add(rand, MiddleColumn);

        do
        {
            rand = Random.Range(0, 9999);
        }
        while (mapColumn.ContainsKey(rand));
        mapColumn.Add(rand, RightColumn);

		foreach (KeyValuePair<int, GameObject> kvp in mapColumn)
		{
			kvp.Value.SetActive(false);
		}

		//

		int[] keyArray = new int[3];
        var ad = mapColumn.Keys;
        ad.CopyTo(keyArray, 0);
        System.Array.Sort(keyArray);

        int count = 2;//(Random.Range(0, 100) < 30) ? 1 : 2;
        for (int i = 0; i < count; ++i)
        {
            mapColumn[keyArray[i]].SetActive(true);
        }
    }

	public void Rotate()
	{
		transform.Rotate(90f, 0f, 0f, Space.Self);
	}

    #endregion
}

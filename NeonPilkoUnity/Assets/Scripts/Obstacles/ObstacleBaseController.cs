﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBaseController : MonoBehaviour
{
	#region Members

	public Material MimeMaterial;

    protected Vector3 m_forward;
    private float m_forwardSpeed = 1f;

	protected Animator m_anim = null;

	#endregion


	#region Properties

	public static float GlobalSpeed { get; set; } = 1f;

	#endregion


	#region ObstacleBaseController

	public virtual void Setup(Vector3 start, Vector3 end, float speed)
    {
        m_forward = end - start;
        m_forward.Normalize();
        m_forwardSpeed = speed;

		if (SaveProfile.Instance.MimeMode)
		{
			MeshRenderer[] meshes = GetComponentsInChildren<MeshRenderer>();
			foreach (MeshRenderer mr in meshes)
			{
				mr.material = MimeMaterial;
			}
			//
			Light light = GetComponentInChildren<Light>();
			light.color = Color.white;
		}
    }


    public virtual void FadeOut()
    {
        IEnumerator coroutine = FadeObstacleCoroutine(false);
        StartCoroutine(coroutine);
    }


    public virtual void FadeIn()
    {
        IEnumerator coroutine = FadeObstacleCoroutine(true);
        StartCoroutine(coroutine);
    }

	public virtual void SlowMotion(bool slowMotion, float mul)
	{
		StopAllCoroutines();
		StartCoroutine(SlowMotionCoroutine(slowMotion, mul));
	}

	public virtual void PauseObstacle()
	{
		m_anim.speed = 0f;
		GlobalSpeed = 0f;
	}

	#endregion


	#region MonoBehaviour

	protected void Awake()
	{
		m_anim = GetComponent<Animator>();
	}

	protected void Start()
    {
        FadeIn();
    }


    protected void Update()
    {
		float t = Time.deltaTime * GlobalSpeed;
        transform.position = transform.position + (m_forward * m_forwardSpeed * t);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            FadeOut();
        }
    }

	#endregion


	#region Coroutine

	private IEnumerator FadeObstacleCoroutine(bool fadeIn)
	{
		MeshRenderer[] meshes = GetComponentsInChildren<MeshRenderer>();
		Light light = GetComponentInChildren<Light>();

		const float fadeValue = 0.9f;
        float alpha = (fadeIn) ? 0f : fadeValue;
        float target = (fadeIn) ? fadeValue : 0f;
        float sign = (fadeIn) ? 1f : -1f;
        float speed = (fadeIn) ? 1f : 2f;
		float lightIntensity = light.intensity;

        do
        {
            alpha += (Time.deltaTime * speed * sign);
			if (fadeIn)
				alpha = Mathf.Min(alpha, target);
			else
				alpha = Mathf.Max(alpha, target);

			//

			foreach (MeshRenderer mr in meshes)
            {
                Color c = mr.materials[0].color;
                c.a = alpha;
                mr.materials[0].color = c;
            }

			//
			light.intensity = Mathf.Lerp(0f, lightIntensity, alpha);

			//
			yield return new WaitForEndOfFrame();

        } while (alpha != target);

		//

		if (!fadeIn)
			Destroy(gameObject);
	}

	private IEnumerator SlowMotionCoroutine(bool slowMotion, float mul)
	{
		float t = 0f;
		while (true)
		{
			t = Time.deltaTime * mul;
			if (slowMotion)
			{
				GlobalSpeed = Mathf.Lerp(0f, 1f, GlobalSpeed - t);
				m_anim.speed = GlobalSpeed;

				if (GlobalSpeed == 0f)
					break;
			}
			else
			{
				GlobalSpeed = Mathf.Lerp(0f, 1f, GlobalSpeed + t);
				m_anim.speed = GlobalSpeed;

				if (GlobalSpeed == 1f)
					break;
			}
			yield return new WaitForEndOfFrame();
		}
	}

    #endregion
}

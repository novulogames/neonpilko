﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	#region Properties

	public AudioSource UISource;
	[Space]
	public AudioClip PressSound;
	public AudioClip CancelSound;
	public AudioClip ToggleSound;

	#endregion


	#region Callback

	public void PlayPressSound()
	{
		if (!SaveProfile.Instance.SFX)
			return;

		UISource.PlayOneShot(PressSound);
	}

	public void PlayCancelSound()
	{
		if (!SaveProfile.Instance.SFX)
			return;

		UISource.PlayOneShot(CancelSound);
	}

	public void PlayToggleSound()
	{
		if (!SaveProfile.Instance.SFX)
			return;

		UISource.PlayOneShot(ToggleSound);
	}

	#endregion
}

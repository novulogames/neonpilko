﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Math
{
    public class Point2<T>
    {
        #region Members

        public T p1;
        public T p2;

        #endregion
        

        #region Constructor

        public Point2()
        {
            p1 = default(T);
            p2 = default(T);
        }

        public Point2(T _p1, T _p2)
        {
            p1 = _p1;
            p2 = _p2;
        }

        #endregion
    }
}
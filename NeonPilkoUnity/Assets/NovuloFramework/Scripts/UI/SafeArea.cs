﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Novulo.UI
{
    public class SafeArea : MonoBehaviour
    {
        #region Members

        public RectTransform Left;
        public RectTransform Right;
        public RectTransform Top;
        public RectTransform Bottom;

        #endregion


        #region MonoBehaviour

        private void Awake()
        {
            if (!Debug.isDebugBuild)
                Destroy(gameObject);
        }

        private void Start()
        {
            Rect rect = Screen.safeArea;
            Vector2 pos = Vector2.zero;

            //
            pos = Vector2.zero;
            pos.y = rect.center.x - (rect.width * 0.5f);
            Left.position = pos;

            //
            pos = Vector2.zero;
            pos.x = rect.center.x + (rect.width * 0.5f);
            Right.position = pos;

            //
            pos = Vector2.zero;
            pos.y = rect.center.y + (rect.height * 0.5f);
            Top.position = pos;

            //
            pos = Vector2.zero;
            pos.y = rect.center.y - (rect.height * 0.5f);
            Bottom.position = pos;
        }

        #endregion
    }
}
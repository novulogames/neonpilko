﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.UI
{
    public class ToggleSwitch : MonoBehaviour
    {
        #region Members

        public GameObject OnImage;
        public GameObject OffImage;

        private bool m_isOn = false;

        #endregion


        #region Properties

        public bool IsOn
        {
            get { return m_isOn; }
            set
            {
                m_isOn = value;
                OnImage.SetActive(m_isOn);
                OffImage.SetActive(!m_isOn);
            }
        }

        #endregion
    }
}
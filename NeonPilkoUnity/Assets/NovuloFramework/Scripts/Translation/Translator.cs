﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Localisation
{
	public class Translator : MonoBehaviour
	{
		#region Types

		public enum ELang
		{
			EO = 0,
			EN,
			PT,
			FR,

			Noof
		}

		#endregion


		#region Members

		public TextAsset LocDB;

		private ELang m_currentLanguage;

		private Dictionary<string, string> m_loc = null;

		#endregion


		#region Properties

		public static Translator Get { get; private set; } = null;

		#endregion


		#region MonoBehaviour

		void Awake()
		{
			Get = this;
		}

		private void OnDestroy()
		{
			Get = null;
		}

		#endregion


		#region Methods

		public string GetTranslation(string ID)
		{
			if (LocDB == null)
				return string.Format("ID - {0}", ID);

			if (!m_loc.ContainsKey(ID))
				return string.Format("MISSING - {0}", ID);

			string t = m_loc[ID];
			if (string.IsNullOrEmpty(t))
				return string.Format("EMPTY - {0}", ID);

			return t;
		}

		#endregion


		#region Internal

		// Force to avoid langauge scree
		public void SetLanguage(ELang language)
		{
			m_currentLanguage = language;
			if (m_currentLanguage == ELang.Noof)
			{
				switch (Application.systemLanguage)
				{
					case SystemLanguage.English: m_currentLanguage = ELang.EN; break;
					case SystemLanguage.French: m_currentLanguage = ELang.FR; break;
					case SystemLanguage.Portuguese: m_currentLanguage = ELang.PT; break;
					default: m_currentLanguage = ELang.EN; break;
				}
			}

			//
			LoadLoc();
		}

		private void LoadLoc()
		{
			if (LocDB == null)
				return;

			//

			string tag = GetLanguageTag();

			Novulo.Utility.JSONNode root = Novulo.Utility.JSON.Parse(LocDB.text);
			Novulo.Utility.JSONArray locs = root["loc"].AsArray;
			int count = locs.Count;

			m_loc = new Dictionary<string, string>();
			for (int idx = 0; idx < count; ++idx)
			{
				Novulo.Utility.JSONNode node = locs[idx];

				string id = node["id"].Value;
				string value = node[tag].Value;

				m_loc.Add(id, value);
			}
		}

		private string GetLanguageTag()
		{
			switch (m_currentLanguage)
			{
				case ELang.EO: return "eo";
				case ELang.EN: return "en";
				case ELang.FR: return "fr";
				case ELang.PT: return "pt";
			}

			// Missing language
			return "ID";
		}

		#endregion
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

namespace Novulo.Social
{
    public class NativeRateController : MonoBehaviour
    {
        #region Types

        private enum State
        {
            Wait,
            Open,
            Close,

            Noof
        }

        #endregion


        #region Members

        public string StoreURL;

        private State m_state = State.Noof;

        #endregion


        #region MonoBehaviour

        private IEnumerator UpdateCoroutine()
        {
            yield return new WaitForEndOfFrame();

            m_state = State.Wait;
            while (true)
            {
                switch (m_state)
                {
                    case State.Open:
                        {
                            Application.OpenURL(StoreURL);
                            yield break;
                        }

                    case State.Close:
                        {
                            yield break;
                        }

                    case State.Wait:
                        {
                            yield return new WaitForSeconds(0.1f);
                            break;
                        }
                }
            }
        }

        #endregion


        #region Internal

        private void ShowNativeShare()
        {
#if UNITY_EDITOR

            return;

#elif UNITY_IOS
            
		    Device.RequestStoreReview ();

#elif UNITY_ANDROID

            // Start update coroutine
            StartCoroutine(UpdateCoroutine());

            //

            // Obtain activity
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            // Lets execute the code in the UI thread
            activity.Call("runOnUiThread", new AndroidJavaRunnable(() => {

                // Create an AlertDialog.Builder object
                AndroidJavaObject alertDialogBuilder = new AndroidJavaObject("android/app/AlertDialog$Builder", activity);

                // Call setMessage on the builder
                alertDialogBuilder.Call<AndroidJavaObject>("setMessage", "Would you like to rate us?");

                // Call setCancelable on the builder
                alertDialogBuilder.Call<AndroidJavaObject>("setCancelable", true);

                // Call setPositiveButton and set the message along with the listner
                // Listner is a proxy class
                alertDialogBuilder.Call<AndroidJavaObject>("setPositiveButton", "Rate", new PositiveButtonListner(this));

                // Call setPositiveButton and set the message along with the listner
                // Listner is a proxy class
                alertDialogBuilder.Call<AndroidJavaObject>("setNegativeButton", "Later", new NegativeButtonListner(this));

                // Finally get the dialog instance and show it
                AndroidJavaObject dialog = alertDialogBuilder.Call<AndroidJavaObject>("create");
                dialog.Call("show");
            }));
#endif
        }

        #endregion


        #region Callback

#if UNITY_ANDROID
        private class PositiveButtonListner : AndroidJavaProxy
        {
            private NativeRateController mDialog;

            public PositiveButtonListner(NativeRateController d)
             : base("android.content.DialogInterface$OnClickListener")
            {
                mDialog = d;
            }

            public void onClick(AndroidJavaObject obj, int value)
            {
                mDialog.m_state = State.Open;
            }
        }

        private class NegativeButtonListner : AndroidJavaProxy
        {
            private NativeRateController mDialog;

            public NegativeButtonListner(NativeRateController d)
            : base("android.content.DialogInterface$OnClickListener")
            {
                mDialog = d;
            }

            public void onClick(AndroidJavaObject obj, int value)
            {
                mDialog.m_state = State.Close;
            }
        }
#endif

        #endregion
    }
}
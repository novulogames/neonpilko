﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Adverts
{
	public class AdsManager : MonoBehaviour
	{
		#region Events

		public delegate void AdStartedUserCallback();
		public delegate void AdFinishedUserCallbck(AdResult result);

		#endregion


		#region Types

		public enum AdType
		{
			Reward = 0,
			Video,
			Banner,

			Noof
		}

		public enum AdResult
		{
			Finished = 0,
			Skipped,
			Failed,

			Noof
		}

		#endregion


		#region Members

		public string iOSGameID = "";
		public string AndroidGameID = "";

		private string m_gameID = "";

		private Dictionary<AdType, string> m_adsNameMap = new Dictionary<AdType, string>()
		{
			{ AdType.Reward, "rewardedVideo" },
			{ AdType.Video, "video" },
			{ AdType.Banner, "banner" },
		};

		private UnityEngine.Monetization.ShowAdCallbacks m_adCallbacks;
		private AdStartedUserCallback m_adStartedUserCallback = null;
		private AdFinishedUserCallbck m_adFinishedUserCallback = null;

		#endregion


		#region Properties

		public static AdsManager Get { get; private set; } = null;

		public bool Initialised
		{
			get { return UnityEngine.Monetization.Monetization.isInitialized; }
		}

		#endregion


		#region MonoBehaviour

		void Awake()
		{
#if UNITY_IOS
			m_gameID = iOSGameID;
#elif UNITY_ANDROID
			m_gameID = AndroidGameID;
#endif
			//
			m_adCallbacks = new UnityEngine.Monetization.ShowAdCallbacks
			{
				startCallback = AdStart,
				finishCallback = AdFinish
			};
			//
			Get = this;
		}

		void OnDestroy()
		{
			Get = null;
		}

		private void Start()
		{
			InitAds();
		}

		#endregion


		#region Methods

		#region GDPR

		public void GiveGDPRConcent(bool give)
		{
			UnityEngine.Monetization.MetaData monetizationGDPRMetaData = new UnityEngine.Monetization.MetaData("gdpr");
			monetizationGDPRMetaData.Set("consent", give ? "true" : "false");
			UnityEngine.Monetization.Monetization.SetMetaData(monetizationGDPRMetaData);

			UnityEngine.Advertisements.MetaData advertisementGDPRMetaData = new UnityEngine.Advertisements.MetaData("gdpr");
			advertisementGDPRMetaData.Set("consent", give ? "true" : "false");
			UnityEngine.Advertisements.Advertisement.SetMetaData(advertisementGDPRMetaData);
		}

		#endregion


		#region Ads

		public bool IsAdReady(AdType type)
		{
			string placementId = m_adsNameMap[type];
			return UnityEngine.Monetization.Monetization.IsReady(placementId);
		}

		public void ShowAd(AdType type, AdStartedUserCallback startCallback, AdFinishedUserCallbck finishCallback)
		{
			// Get placement
			string placementId = m_adsNameMap[type];
			UnityEngine.Monetization.ShowAdPlacementContent ad = UnityEngine.Monetization.Monetization.GetPlacementContent(placementId) as UnityEngine.Monetization.ShowAdPlacementContent;
			Debug.Assert(ad != null, "Ad placement content is null");

			// Set callback
			m_adStartedUserCallback = startCallback;
			m_adFinishedUserCallback = finishCallback;

			// Show ad
			ad.Show(m_adCallbacks);
		}

		public string GetPlacementID(AdType type)
		{
			return m_adsNameMap[type];
		}

		#endregion

		#region Banner

		public void LoadBanner()
		{
			string placementId = m_adsNameMap[AdType.Banner];
			UnityEngine.Advertisements.Advertisement.Banner.Load(placementId);
		}

		public bool IsBannerReady()
		{
			string placementId = m_adsNameMap[AdType.Banner];
			return UnityEngine.Advertisements.Advertisement.IsReady(placementId);
		}

		public bool IsBannerShowing()
		{
			return UnityEngine.Advertisements.Advertisement.isShowing;
		}

		public void ShowBanner()
		{
			string placementId = m_adsNameMap[AdType.Banner];
			UnityEngine.Advertisements.Advertisement.Banner.Show(placementId);
		}

		public void HideBanner()
		{
			UnityEngine.Advertisements.Advertisement.Banner.Hide();
		}

		#endregion

		#endregion


		#region Internal

		private void InitAds()
		{
			bool testAds = false;
#if DEBUG
			testAds = true;
#endif
			if (UnityEngine.Monetization.Monetization.isSupported)
				UnityEngine.Monetization.Monetization.Initialize(m_gameID, testAds);

			if (UnityEngine.Advertisements.Advertisement.isSupported)
				UnityEngine.Advertisements.Advertisement.Initialize(m_gameID, testAds);
		}

		#endregion


		#region Callbacks

		private void AdStart()
		{
			if (m_adStartedUserCallback != null)
				m_adStartedUserCallback();

			m_adStartedUserCallback = null;
		}

		private void AdFinish(UnityEngine.Monetization.ShowResult result)
		{
			if (m_adFinishedUserCallback != null)
			{
				AdResult adResult = AdResult.Noof;
				switch (result)
				{
					case UnityEngine.Monetization.ShowResult.Finished: adResult = AdResult.Finished; break;
					case UnityEngine.Monetization.ShowResult.Failed: adResult = AdResult.Failed; break;
					case UnityEngine.Monetization.ShowResult.Skipped: adResult = AdResult.Skipped; break;
					default: break;
				}
				m_adFinishedUserCallback(adResult);
			}

			m_adFinishedUserCallback = null;
		}

		#endregion
	}
}
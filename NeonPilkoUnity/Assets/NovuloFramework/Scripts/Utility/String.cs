﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Novulo.Utility
{
    public class String
    {
        public static string ByteArrayToHexString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }

        public static string StringToHexString(string s)
        {
            byte[] ba = System.Text.Encoding.UTF8.GetBytes(s);

            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }

        public static string StringToBase64Encoding(string s)
        {
            byte[] bytesData = System.Text.Encoding.UTF8.GetBytes(s);
            return System.Convert.ToBase64String(bytesData);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Utility
{
    public class DontDestroy : MonoBehaviour
    {
        #region MonoBehaviour

        void Awake()
        {
            DontDestroyOnLoad(this);
        }

        #endregion
    }
}
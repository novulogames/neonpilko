﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Utility
{
    public class DebugOnly : MonoBehaviour
    {
        #region MonoBehaviour

#if !UNITY_EDITOR
        private void Awake()
        {
            if (!Debug.isDebugBuild)
                Destroy(gameObject);
    }
#endif

        #endregion
    }
}
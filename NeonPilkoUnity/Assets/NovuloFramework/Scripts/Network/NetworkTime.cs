﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace Novulo.Network
{
    public class NetworkTime
    {
        public static DateTime GetDate()
        {
            try
            {
                byte[] ntpData = new byte[48];

                //LeapIndicator = 0 (no warning), VersionNum = 3 (IPv4 only), Mode = 3 (Client Mode)
                ntpData[0] = 0x1B;

                IPAddress[] addresses = Dns.GetHostEntry("pool.ntp.org").AddressList;
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                socket.Connect(new IPEndPoint(addresses[0], 123));
                socket.ReceiveTimeout = 1000;

                socket.Send(ntpData);
                socket.Receive(ntpData);
                socket.Close();

                ulong intc = (ulong)ntpData[40] << 24 | (ulong)ntpData[41] << 16 | (ulong)ntpData[42] << 8 | (ulong)ntpData[43];
                ulong frac = (ulong)ntpData[44] << 24 | (ulong)ntpData[45] << 16 | (ulong)ntpData[46] << 8 | (ulong)ntpData[47];

                double ms = (double)((intc * 1000) + ((frac * 1000) / 0x100000000L));
                TimeSpan ts = TimeSpan.FromMilliseconds(ms);
                return new DateTime(1900, 1, 1) + ts;
            }
            catch (Exception exception)
            {
                Debug.Log("Could not get NTP time");
                Debug.Log(exception);
                return DateTime.Now;
            }
        }
    }
}
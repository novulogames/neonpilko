﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

namespace Novulo.Store
{
    public class IAPManager : MonoBehaviour, IStoreListener
    {
        #region Types

        [System.Serializable]
        public enum IAPType
        {
            Consumable,
            NonConsumable,
            Subscription,

            Noof
        }

        [System.Serializable]
        public class IAPDef
        {
            public IAPType type;
            public string ID;
        }

        #endregion


        #region Events

        public delegate void IAPStoreInit(bool success);
        public static IAPStoreInit OnStoreInit;

        public delegate void IAPPurchaseCompleteCallback(string IAPName, bool success);
        public static IAPPurchaseCompleteCallback OnIAPPurchaseComplete;

        public delegate void IAPPurchasStartedCallback(string IAPName);
        public static IAPPurchasStartedCallback OnIAPPurchaseStarted;

        #endregion


        #region Members

        public List<IAPDef> StoreDefinition;

        private IStoreController m_controller = null;
#if UNITY_IOS
        private IAppleExtensions m_extensions = null;
#endif
        private ConfigurationBuilder m_builder = null;

		#endregion


		#region Properties

		public static IAPManager Get { get; private set; } = null;

        public bool IsInitialised
        {
            get { return (m_controller != null); }
        }

        #endregion


        #region MonoBehaviour

        private void Awake()
        {
            Get = this;
            //
            m_builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        }

        private void OnDestroy()
        {
            Get = null;
        }

        #endregion


        #region IStoreListener

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            Debug.Log("Novulo.IAPManager: Init success");

            m_controller = controller;
#if UNITY_IOS
            if (extensions != null)
                m_extensions = extensions.GetExtension<IAppleExtensions>();
#endif

            //
            if (OnStoreInit != null)
                OnStoreInit(true);
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("Novulo.IAPManager: Init failed");

            switch (error)
            {
                case InitializationFailureReason.AppNotKnown: Debug.Log("App not known"); break;
                case InitializationFailureReason.NoProductsAvailable: Debug.Log("No products available"); break;
                case InitializationFailureReason.PurchasingUnavailable: Debug.Log("Purchasing unavailable"); break;
            }

            if (OnStoreInit != null)
                OnStoreInit(false);
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            Debug.Log("Novulo.IAPManager: Purchase completed: " + e.purchasedProduct.definition.storeSpecificId);

            if (OnIAPPurchaseComplete != null)
                OnIAPPurchaseComplete(e.purchasedProduct.definition.storeSpecificId, true);

            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product i, PurchaseFailureReason error)
        {
            string productID = (i == null) ? "" : i.definition.storeSpecificId;

            Debug.Log("Novulo.IAPManager: Purchase failed");
            switch (error)
            {
                case PurchaseFailureReason.UserCancelled: Debug.Log("User cancelled"); break;
                case PurchaseFailureReason.SignatureInvalid: Debug.Log("Signature invalid"); break;
                case PurchaseFailureReason.PurchasingUnavailable: Debug.Log("Purchasing unavailable"); break;
                case PurchaseFailureReason.ProductUnavailable: Debug.Log("Product unavailable"); break;
                case PurchaseFailureReason.PaymentDeclined: Debug.Log("Payment declined"); break;
                case PurchaseFailureReason.ExistingPurchasePending: Debug.Log("Existing payment pending"); break;
                case PurchaseFailureReason.DuplicateTransaction: Debug.Log("Duplicate transation"); break;
                case PurchaseFailureReason.Unknown: Debug.Log("Unknown error"); break;
            }

            //
            if (OnIAPPurchaseComplete != null)
                OnIAPPurchaseComplete(productID, false);
        }

        public void RestorePurchases()
        {
            if (!IsInitialised)
            {
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

#if UNITY_IOS
            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                Debug.Log("Novulo.IAPManager: RestorePurchases started ...");
            
                //
                m_extensions.RestoreTransactions((result) => {
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });
            }
            else
#endif
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.Log("Novulo.IAPManager: RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);

            }
        }

		#endregion


		#region Methods

		public void Init()
		{
			int total = StoreDefinition.Count;
			for (int idx = 0; idx < total; ++idx)
			{
				IAPDef def = StoreDefinition[idx];
				//
				ProductType productType = ProductType.Consumable;
				switch (def.type)
				{
					case IAPType.Consumable: productType = ProductType.Consumable; break;
					case IAPType.NonConsumable: productType = ProductType.NonConsumable; break;
					case IAPType.Subscription: productType = ProductType.Subscription; break;
				}
				// Register IAP in Unity system
				m_builder.AddProduct(def.ID, productType);
			}

			// Initialize
			UnityPurchasing.Initialize(this, m_builder);
		}

		public void Purchase(string ID)
        {
            if (!IsInitialised)
            {
                OnPurchaseFailed(null, PurchaseFailureReason.Unknown);
                return;
            }

            //

            Product product = m_controller.products.WithID(ID);
            if (product == null)
            {
                Debug.Assert(false, "Product ID not found");
                return;
            }

            if (OnIAPPurchaseStarted != null)
                OnIAPPurchaseStarted(ID);

            m_controller.InitiatePurchase(product);
        }

        public ProductMetadata GetProductInfo(string ID)
        {
            if (!IsInitialised)
                return null;

            return m_controller.products.WithStoreSpecificID(ID).metadata;
        }

        public ProductDefinition GetProductDefinition(string ID)
        {
            if (!IsInitialised)
                return null;

            return m_controller.products.WithStoreSpecificID(ID).definition;
        }

        #endregion

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Platform.Amazon
{
    public class AmazonController : MonoBehaviour
    {
        #region MonoBehaviour

#if UNITY_AMAZON
        private void Awake()
        {
            Destroy(gameObject);
        }
#endif

        #endregion
    }
}
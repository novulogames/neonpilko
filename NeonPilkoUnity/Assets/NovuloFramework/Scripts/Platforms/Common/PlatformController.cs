﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Platform.Common
{
	public class PlatformController : MonoBehaviour
	{
		#region Members

		[Header("Allow for:")]
		public bool Android;
		public bool iOS;
		public bool StandAlone;

		#endregion


		#region MonoBehaviour

		private void Awake()
		{
#if UNITY_ANDROID
			if (!Android)
#elif UNITY_IOS
			if (!iOS)
#else
			if (!StandAlone)
#endif
			{
				Destroy(gameObject);
			}
		}
	#endregion
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Platform.Common
{
    public interface IBackButtonListener
    {
        void HandleBackButton();
    }
}
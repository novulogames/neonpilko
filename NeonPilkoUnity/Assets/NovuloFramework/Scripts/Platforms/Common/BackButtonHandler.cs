﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Novulo.Platform.Common
{ 
    public class BackButtonHandler : MonoBehaviour
    {
        #region Members

        private List<IBackButtonListener> m_listeners = new List<IBackButtonListener>();

        #endregion


        #region Properties

        public static BackButtonHandler Get { get; private set; } = null;

        #endregion


        #region MonoBehaviour

        private void Awake()
        {
            Get = this;
        }

        private void OnDestroy()
        {
            Get = null;
            //
            m_listeners.Clear();
        }

        private void Start()
        {
            // Enabled when first listener is registered
            this.enabled = false;
        }

        private void Update()
        {
            if (m_listeners.Count == 0)
                return;

            if (Input.GetKeyUp(KeyCode.Escape))
            {
                // Last listener has priority
                int index = m_listeners.Count - 1;
                Debug.Assert(m_listeners[index] != null, "IBackButtonListener destroy before unregistering");
                m_listeners[index].HandleBackButton();
            }
        }

        #endregion


        #region Methods

        public void RegisterListener(IBackButtonListener listener)
        {
            if (m_listeners.Contains(listener))
                return;

            // Add to back == highest priority
            m_listeners.Add(listener);

            // Enable if listeners
            this.enabled = (m_listeners.Count > 0);
        }

        public void UnregisterListener(IBackButtonListener listener)
        {
            if (!m_listeners.Contains(listener))
                return;

            m_listeners.Remove(listener);

            // Disable if no listeners
            this.enabled = (m_listeners.Count > 0);
        }

        #endregion
    }
}
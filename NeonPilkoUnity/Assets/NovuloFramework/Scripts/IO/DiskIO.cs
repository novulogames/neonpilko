﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Novulo.IO
{
    public class DiskIO
    {
        #region Methods

        public static void WriteData<T>(string fullPath, T data)
        {
            if (System.IO.File.Exists(fullPath) == false)
                System.IO.File.Create(fullPath).Close();

            // Write to disk
            BinaryFormatter bf = new BinaryFormatter();
            System.IO.FileStream fs = System.IO.File.OpenWrite(fullPath);
            bf.Serialize(fs, (object)data);
            fs.Close();
        }

        public static T ReadData<T>(string fullPath)
        {
            if (System.IO.File.Exists(fullPath) == false)
                return default(T);

            // Read from disk
            BinaryFormatter bf = new BinaryFormatter();
            System.IO.FileStream fs = null;
            object data = null;
            {
                try
                {
                    fs = System.IO.File.OpenRead(fullPath);
                    data = bf.Deserialize(fs);
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning("Error reading data from disk: " + e.Message);
                }
            }
            fs?.Close();

            return (T)data;
        }

        public static void DeleteData(string fullPath)
        {
            if (System.IO.Directory.Exists(fullPath))
                System.IO.Directory.Delete(fullPath, true);
        }

        #endregion
    }
}